
# Areas

## Description

This mod allows you to select areas for each world, and you can do something
using them.

## Features

This mod adds one command:  
**Flattened**: `/areas version|(list [(notify_entering|notify_leaving
[all|console|trigger])|(entities_in|entities_out_of
<entities>)]|(pos_belongs_to|pos_not_belong_to [<pos>]
[<dimension>]))|(create|delete <area_name>)|(rename|clone <area_name>
<new_area_name>)|((intersect|union|difference|difference_by|sym_difference)
<area_name> (cuboid [<pos1>] [<pos2>] [<dimension>])|(prism <pos1> <pos2>
<pos3> [<dimension>])|(ellipsoid <center_pos> <axes_lengths> [<include_face>]
[<dimension>])|(cylinder <bottom_center_pos> <axes_lengths>
[<include_side_face>] [<dimension>]))|(notify_entering|notify_leaving
<area_name> [all|console|trigger|off])|(test (<pos> belongs_to|not_belong_to
<area_name>)|(<entities> in|not_in <area_name>))|(color <area_name>
unset|<intrgb>)`

**Tree**:
```
/areas
|
+---version
|
+---list
|   \
|   |---notify_entering
|   |   \
|   |   |---all
|   |   \
|   |   |---console
|   |   \
|   |    ---trigger
|   \
|   |---notify_leaving
|   |   \
|   |   |---all
|   |   \
|   |   |---console
|   |   \
|   |    ---trigger
|   \
|   |---entities_in
|   |   |
|   |   +---<entities>
|   \
|   |---entities_out_of
|   |   |
|   |   +---<entities>
|   \
|   |---pos_belongs_to
|   |   \
|   |    ---<pos>
|   |       \
|   |        ---<dimension>
|   \
|    ---pos_not_belong_to
|       \
|        ---<pos>
|           \
|            ---<dimension>    
|
+---create
|   |
|   +---<area_name>
|
+---delete
|   |
|   +---<area_name>
|
+---rename
|   |
|   +---<area_name>
|       |
|       +---<new_area_name>
|
+---clone
|   |
|   +---<area_name>
|       |
|       +---<new_area_name>
|
+---intersect
|   |
|   +---cuboid
|   |   \
|   |    ---<pos1>
|   |       \
|   |        ---<pos2>
|   |           \
|   |            ---<dimension>
|   |
|   +---prism
|   |   |
|   |   +---<pos1>
|   |       |
|   |       +---<pos2>
|   |           |
|   |           +---<pos3>
|   |               \
|   |                ---<dimension>
|   |
|   +---ellipsoid
|   |   |
|   |   +---<center_pos>
|   |       |
|   |       +---<axes_lengths>
|   |           \
|   |            ---<include_face>
|   |               \
|   |                ---<dimension>
|   |
|   +---cylinder
|       |
|       +---<bottom_center_pos>
|           |
|           +---<axes_lengths>
|               \
|                ---<include_side_face>
|                   \
|                    ---<dimension>
|
+---union
|   |
|   +---cuboid
|   |   \
|   |    ---<pos1>
|   |       \
|   |        ---<pos2>
|   |           \
|   |            ---<dimension>
|   |
|   +---prism
|   |   |
|   |   +---<pos1>
|   |       |
|   |       +---<pos2>
|   |           |
|   |           +---<pos3>
|   |               \
|   |                ---<dimension>
|   |
|   +---ellipsoid
|   |   |
|   |   +---<center_pos>
|   |       |
|   |       +---<axes_lengths>
|   |           \
|   |            ---<include_face>
|   |               \
|   |                ---<dimension>
|   |
|   +---cylinder
|       |
|       +---<bottom_center_pos>
|           |
|           +---<axes_lengths>
|               \
|                ---<include_side_face>
|                   \
|                    ---<dimension>
|
+---difference
|   |
|   +---cuboid
|   |   \
|   |    ---<pos1>
|   |       \
|   |        ---<pos2>
|   |           \
|   |            ---<dimension>
|   |
|   +---prism
|   |   |
|   |   +---<pos1>
|   |       |
|   |       +---<pos2>
|   |           |
|   |           +---<pos3>
|   |               \
|   |                ---<dimension>
|   |
|   +---ellipsoid
|   |   |
|   |   +---<center_pos>
|   |       |
|   |       +---<axes_lengths>
|   |           \
|   |            ---<include_face>
|   |               \
|   |                ---<dimension>
|   |
|   +---cylinder
|       |
|       +---<bottom_center_pos>
|           |
|           +---<axes_lengths>
|               \
|                ---<include_side_face>
|                   \
|                    ---<dimension>
|
+---difference_by
|   |
|   +---cuboid
|   |   \
|   |    ---<pos1>
|   |       \
|   |        ---<pos2>
|   |           \
|   |            ---<dimension>
|   |
|   +---prism
|   |   |
|   |   +---<pos1>
|   |       |
|   |       +---<pos2>
|   |           |
|   |           +---<pos3>
|   |               \
|   |                ---<dimension>
|   |
|   +---ellipsoid
|   |   |
|   |   +---<center_pos>
|   |       |
|   |       +---<axes_lengths>
|   |           \
|   |            ---<include_face>
|   |               \
|   |                ---<dimension>
|   |
|   +---cylinder
|       |
|       +---<bottom_center_pos>
|           |
|           +---<axes_lengths>
|               \
|                ---<include_side_face>
|                   \
|                    ---<dimension>
|
+---sym_difference
|   |
|   +---cuboid
|   |   \
|   |    ---<pos1>
|   |       \
|   |        ---<pos2>
|   |           \
|   |            ---<dimension>
|   |
|   +---prism
|   |   |
|   |   +---<pos1>
|   |       |
|   |       +---<pos2>
|   |           |
|   |           +---<pos3>
|   |               \
|   |                ---<dimension>
|   |
|   +---ellipsoid
|   |   |
|   |   +---<center_pos>
|   |       |
|   |       +---<axes_lengths>
|   |           \
|   |            ---<include_face>
|   |               \
|   |                ---<dimension>
|   |
|   +---cylinder
|       |
|       +---<bottom_center_pos>
|           |
|           +---<axes_lengths>
|               \
|                ---<include_side_face>
|                   \
|                    ---<dimension>
|
+---notify_entering
|   |
|   +---<area_name>
|       \
|       |---all
|       \
|       |---console
|       \
|       |---trigger
|       \
|        ---off
|
+---notify_leaving
|   |
|   +---<area_name>
|       \
|       |---all
|       \
|       |---console
|       \
|       |---trigger
|       \
|        ---off
|
+---test
|   |
|   +---<pos>
|   |   |
|   |   +---belongs_to
|   |   |   |
|   |   |   +---<area_name>
|   |   |
|   |   +---not_belong_to
|   |       |
|   |       +---<area_name>
|   |
|   +---<entities>
|       |
|       +---in
|       |   |
|       |   +---<area_name>
|       |
|       +---not_in
|           |
|           +---<area_name>
|
+---color
    |
    +---<area_name>
        \
        |---unset
        \
         ---<intrgb>
```

This mod adds one entity selector argument:
E.g. `@e[area="Area 1",area="Area 2",area=!"Area 3"]` will select all entities
in both `Area 1` and `Area 2`, and not in `Area 3`.

This mod adds one `/execute`-conditional-predicate:
E.g. `/execute if area "Area 1" contains ~ ~ ~ run say In Area 1` will execute
`say In Area 1` if the position of the executor belongs to `Area 1`.
E.g.
`/execute if area "Area 2" keeps_any @a run say At least 1 player in Area 2`
will execute `say At least 1 player in Area 2` if there exists a player in
`Area 2`.
E.g.
`/execute if area "Area 3" keeps_all @a run say No player is out of Area 3`
will execute `say No player is out of Area 3` if all players are in `Area 3`.
Note that if there is no player selected, this condition will pass.

## Roadmap

LMT...

<font size=1>Do not be confused with [Areas](https://github.com/IAmREGE/Areas)
here.</font>
