package rege.rege.minecraftmod.areas;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.packet.s2c.play.OverlayMessageS2CPacket;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.MutableText;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.util.WorldSavePath;
import net.minecraft.util.math.Vec3d;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.jetbrains.annotations.Range;
import rege.rege.minecraftmod.NoMc;
import rege.rege.misc.areas.Area;
import rege.rege.misc.areas.CuboidArea;
import rege.rege.misc.areas.EllipsoidArea;
import rege.rege.misc.areas.EllipticCylinderArea;
import rege.rege.misc.areas.TriangularPrismArea;
import rege.rege.misc.areas.TwoAreasCombination;

import static rege.rege.minecraftmod.areas.AreasMain.LOGGER;
import static rege.rege.minecraftmod.areas.server.command.AreasCommand
              .getDimensionId;

// Authors: REGE
// Since: 0.0.1-a1
public class Areas {
    /**
     * @author REGE
     * @since 0.0.1-a3
     */
    @NoMc
    public static enum NotifyMode {
        /**
         * @author REGE
         * @since 0.0.1-a3
         */
        ALL,
        /**
         * @author REGE
         * @since 0.0.1-a3
         */
        CONSOLE,
        /**
         * @author REGE
         * @since 0.0.1-a3
         */
        TRIGGER;
    }

    // Authors: REGE
    // Since: 0.0.1-a1
    private static final List<Entry<MinecraftServer, Areas>> AREAS_RECORD =
    new ArrayList<>();

    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    @NoMc
    private final Map<String, Area> areas;
    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    @NoMc
    private final Map<String, NotifyMode> notifyEntering;
    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    @NoMc
    private final Map<String, NotifyMode> notifyLeaving;
    // Authors: REGE
    // Since: 0.0.1-a1
    private List<Entry<PlayerEntity, Set<String>>> playerTracking;
    /**
     * @author REGE
     * @since 0.0.1-a5
     */
    @NoMc
    private final Map<String, Integer> color;

    // Authors: REGE
    // Since: 0.0.1-a1
    @Contract(pure = true)
    public Areas() {
        this.areas = new HashMap<>();
        this.notifyEntering = new HashMap<>();
        this.notifyLeaving = new HashMap<>();
        this.playerTracking = new ArrayList<>();
        this.color = new HashMap<>();
    }

    /**
     * Set the state of notify entering for the area.
     * @param areaName The name of the area.
     * @return The previous state of notify entering of the area.
     * @author REGE
     * @since 0.0.1-a3
     */
    @NoMc
    @Nullable
    public NotifyMode setNotifyEntering(String areaName,
                                        @NoMc @Nullable NotifyMode mode) {
        final NotifyMode RES = this.notifyEntering.getOrDefault(areaName,null);
        if (mode != null) {
            this.notifyEntering.put(areaName, mode);
        } else if (RES != null) {
            this.notifyEntering.remove(areaName);
        }
        return RES;
    }

    /**
     * Set the state of notify leaving for the area.
     * @param areaName The name of the area.
     * @return The previous state of notify leaving of the area.
     * @author REGE
     * @since 0.0.1-a3
     */
    @NoMc
    @Nullable
    public NotifyMode setNotifyLeaving(String areaName,
                                       @NoMc @Nullable NotifyMode mode) {
        final NotifyMode RES = this.notifyLeaving.getOrDefault(areaName, null);
        if (mode != null) {
            this.notifyLeaving.put(areaName, mode);
        } else if (RES != null) {
            this.notifyLeaving.remove(areaName);
        }
        return RES;
    }

    /**
     * @param areaName The name of the area to get color.
     * @return The current color number.
     * @author REGE
     * @since 0.0.1-a5
     */
    @Contract(pure = true)
    @NoMc
    @Nullable
    public Integer getColor(String areaName) {
        return this.color.getOrDefault(areaName, null);
    }

    /**
     * @param areaName The name of the area to set color.
     * @param color The number of #RRGGBB color code converted. {@code null}
     * for resetting.
     * @return The previous color number.
     * @author REGE
     * @since 0.0.1-a5
     */
    @NoMc
    @Nullable
    public Integer setColor(String areaName, @Nullable Integer color) {
        if (color == null) {
            return this.color.containsKey(areaName) ?
                   this.color.remove(areaName) : null;
        }
        return this.color.put(areaName, color);
    }

    // Authors: REGE
    // Since: 0.0.1-a1
    public Areas(MinecraftServer server)
    throws IllegalArgumentException, IOException {
        this();
        try {
            final FileReader FR = new FileReader(server.getSavePath(
                WorldSavePath.ROOT
            ).resolve("areas.json").toFile());
            final StringBuilder SB = new StringBuilder();
            int a;
            while ((a = FR.read()) != -1) {
                SB.append((char)a);
            }
            FR.close();
            final JsonObject JSON =
            new JsonParser().parse(SB.toString()).getAsJsonObject();
            if (JSON.has("areas_def") && JSON.get("areas_def").isJsonObject()){
                this
                .importAreasFromJson(JSON.get("areas_def").getAsJsonObject());
            }
            if (JSON.has("notify_entering")) {
                if (JSON.get("notify_entering").isJsonObject()) {
                    final JsonObject NOTIFY_ENTERING_OBJ =
                    JSON.get("notify_entering").getAsJsonObject();
                    for (String i : NOTIFY_ENTERING_OBJ.keySet()) {
                        final String NOTIFY_MODE_STR =
                        NOTIFY_ENTERING_OBJ.get(i).getAsString().toLowerCase();
                        if (NOTIFY_MODE_STR.equals("console")) {
                            this.notifyEntering.put(i, NotifyMode.CONSOLE);
                        } else {
                            this.notifyEntering.put(
                                i, NOTIFY_MODE_STR.equals("trigger") ?
                                   NotifyMode.TRIGGER : NotifyMode.ALL
                            );
                        }
                    }
                } else if (JSON.get("notify_entering").isJsonArray()) {
                    for (JsonElement i :
                         JSON.get("notify_entering").getAsJsonArray()) {
                        this
                        .notifyEntering.put(i.getAsString(), NotifyMode.ALL);
                    }
                }
            }
            if (JSON.has("notify_leaving")) {
                if (JSON.get("notify_leaving").isJsonObject()) {
                    final JsonObject NOTIFY_LEAVING_OBJ =
                    JSON.get("notify_leaving").getAsJsonObject();
                    for (String i : NOTIFY_LEAVING_OBJ.keySet()) {
                        final String NOTIFY_MODE_STR =
                        NOTIFY_LEAVING_OBJ.get(i).getAsString().toLowerCase();
                        if (NOTIFY_MODE_STR.equals("console")) {
                            this.notifyLeaving.put(i, NotifyMode.CONSOLE);
                        } else {
                            this.notifyLeaving.put(
                                i, NOTIFY_MODE_STR.equals("trigger") ?
                                   NotifyMode.TRIGGER : NotifyMode.ALL
                            );
                        }
                    }
                } else if (JSON.get("notify_leaving").isJsonArray()) {
                    for (JsonElement i :
                         JSON.get("notify_leaving").getAsJsonArray()) {
                        this.notifyLeaving.put(i.getAsString(),NotifyMode.ALL);
                    }
                }
            }
            if (JSON.has("color") && JSON.get("color").isJsonObject()) {
                final JsonObject COLOR_OBJ=JSON.get("color").getAsJsonObject();
                for (String i : COLOR_OBJ.keySet()) {
                    this.color
                    .put(i, Integer.valueOf(COLOR_OBJ.get(i).getAsInt()));
                }
            }
        } catch (FileNotFoundException | IllegalStateException ignored) {}
    }

    /**
     * @return The count of present areas.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    @NoMc
    public @Range(from = 0, to = Integer.MAX_VALUE) int areasCount() {
        return this.areas.size();
    }

    /**
     * @return A {@link java.util.HashSet} contains all present areas with
     * notify entering enabled.
     * @author REGE
     * @since 0.0.1-a1
     * @see #notifyEnteringAreas(NotifyMode)
     */
    @Contract(value = "-> new", pure = true)
    @NoMc
    public Set<String> notifyEnteringAreas() {
        final Set<String> RES = new HashSet<>();
        for (String i : this.areas.keySet()) {
            if (this.notifyEntering.containsKey(i)) {
                RES.add(i);
            }
        }
        return RES;
    }

    /**
     * @param mode The mode to match.
     * @return A {@link java.util.HashSet} contains all present areas with
     * notify entering enabled as the given mode.
     * @author REGE
     * @since 0.0.1-a3
     * @see #notifyEnteringAreas()
     */
    @Contract("_ -> new")
    @NoMc
    public Set<String> notifyEnteringAreas(@NoMc @NotNull NotifyMode mode)
    throws NullPointerException {
        if (mode == null) {
            throw new NullPointerException();
        }
        final Set<String> RES = new HashSet<>();
        for (String i : this.areas.keySet()) {
            if (this.notifyEntering.get(i) == mode) {
                RES.add(i);
            }
        }
        return RES;
    }

    /**
     * @param includeAbsence Whether to include absent areas.
     * @return The count of areas with notify entering enabled.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    @NoMc
    public int notifyEnteringAreasCount(boolean includeAbsence) {
        if (includeAbsence) {
            return this.notifyEntering.size();
        }
        int res = 0;
        for (String i : this.areas.keySet()) {
            if (this.notifyEntering.containsKey(i)) {
                res++;
            }
        }
        return res;
    }

    /**
     * @param mode The mode to match.
     * @param includeAbsence Whether to include absent areas.
     * @return The count of areas with notify entering enabled as the given
     * mode.
     * @author REGE
     * @since 0.0.1-a3
     */
    @NoMc
    public int notifyEnteringAreasCount(@NoMc @NotNull NotifyMode mode,
                                        boolean includeAbsence) {
        if (mode == null) {
            throw new NullPointerException();
        }
        int res = 0;
        if (includeAbsence) {
            for (NotifyMode i : this.notifyEntering.values()) {
                if (i == mode) {
                    res += 1;
                }
            }
            return res;
        }
        for (String i : this.areas.keySet()) {
            if (this.notifyEntering.get(i) == mode) {
                res++;
            }
        }
        return res;
    }

    /**
     * @return A {@link java.util.HashSet} contains all present areas with
     * notify leaving enabled.
     * @author REGE
     * @since 0.0.1-a1
     * @see #notifyLeavingAreas(NotifyMode)
     */
    @Contract(value = "-> new", pure = true)
    @NoMc
    public Set<String> notifyLeavingAreas() {
        final Set<String> RES = new HashSet<>();
        for (String i : this.areas.keySet()) {
            if (this.notifyLeaving.containsKey(i)) {
                RES.add(i);
            }
        }
        return RES;
    }

    /**
     * @param mode The mode to match.
     * @return A {@link java.util.HashSet} contains all present areas with
     * notify leaving enabled as the given mode.
     * @author REGE
     * @since 0.0.1-a3
     * @see #notifyLeavingAreas()
     */
    @Contract("_ -> new")
    @NoMc
    public Set<String> notifyLeavingAreas(@NoMc @NotNull NotifyMode mode)
    throws NullPointerException {
        if (mode == null) {
            throw new NullPointerException();
        }
        final Set<String> RES = new HashSet<>();
        for (String i : this.areas.keySet()) {
            if (this.notifyLeaving.get(i) == mode) {
                RES.add(i);
            }
        }
        return RES;
    }

    /**
     * @param includeAbsence Whether to include absent areas.
     * @return The count of areas with notify leaving enabled.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    @NoMc
    public int notifyLeavingAreasCount(boolean includeAbsence) {
        if (includeAbsence) {
            return this.notifyLeaving.size();
        }
        int res = 0;
        for (String i : this.areas.keySet()) {
            if (this.notifyLeaving.containsKey(i)) {
                res++;
            }
        }
        return res;
    }

    /**
     * @param mode The mode to match.
     * @param includeAbsence Whether to include absent areas.
     * @return The count of areas with notify leaving enabled as the given
     * mode.
     * @author REGE
     * @since 0.0.1-a3
     */
    @NoMc
    public int notifyLeavingAreasCount(@NoMc @NotNull NotifyMode mode,
                                       boolean includeAbsence) {
        if (mode == null) {
            throw new NullPointerException();
        }
        int res = 0;
        if (includeAbsence) {
            for (NotifyMode i : this.notifyLeaving.values()) {
                if (i == mode) {
                    res += 1;
                }
            }
            return res;
        }
        for (String i : this.areas.keySet()) {
            if (this.notifyLeaving.get(i) == mode) {
                res++;
            }
        }
        return res;
    }

    /**
     * @param areaName The area name. Can represent an absent area.
     * @return A boolean reflects whether the area is nofity entering enabled.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    @NoMc
    public boolean isNotifyEntering(String areaName) {
        return this.notifyEntering.containsKey(areaName);
    }

    /**
     * @param areaName The area name. Can represent an absent area.
     * @return The mode of the notify entering, {@code null} if disabled.
     * @author REGE
     * @since 0.0.1-a3
     */
    @Contract(pure = true)
    @NoMc
    @Nullable
    public NotifyMode getNotifyEnteringMode(String areaName) {
        return this.notifyEntering.get(areaName);
    }

    /**
     * @param areaName The area name. Can represent an absent area.
     * @return A boolean reflects whether the area is nofity leaving enabled.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    @NoMc
    public boolean isNotifyLeaving(String areaName) {
        return this.notifyLeaving.containsKey(areaName);
    }

    /**
     * @param areaName The area name. Can represent an absent area.
     * @return The mode of the notify leaving, {@code null} if disabled.
     * @author REGE
     * @since 0.0.1-a3
     */
    @Contract(pure = true)
    @NoMc
    @Nullable
    public NotifyMode getNotifyLeavingMode(String areaName) {
        return this.notifyLeaving.get(areaName);
    }

    /**
     * @return A {@link java.util.HashSet} contains names of all present areas.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    @NoMc
    public Set<String> areaNames() {
        return this.areas.keySet();
    }

    /**
     * @param areaName The name of the area to test.
     * @return A booolean reflects whether the area is present.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    @NoMc
    public boolean has(String areaName) {
        return this.areas.containsKey(areaName);
    }

    /**
     * Serialize this areas to a JSON String.
     * @return The serialized JSON String.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    @NoMc
    public String toJSON() {
        final JsonObject JSON = new JsonObject();
        final JsonObject AREAS_DEF = new JsonObject();
        for (Entry<String, Area> i : this.areas.entrySet()) {
            AREAS_DEF.add(i.getKey(), AreaToJson(i.getValue()));
        }
        JSON.add("areas_def", AREAS_DEF);
        final JsonObject NOTIFY_ENTERING = new JsonObject();
        for (Entry<String, NotifyMode> i : this.notifyEntering.entrySet()) {
            NOTIFY_ENTERING
            .add(i.getKey(), new JsonPrimitive(i.getValue().name()));
        }
        JSON.add("notify_entering", NOTIFY_ENTERING);
        final JsonObject NOTIFY_LEAVING = new JsonObject();
        for (Entry<String, NotifyMode> i : this.notifyLeaving.entrySet()) {
            NOTIFY_LEAVING
            .add(i.getKey(), new JsonPrimitive(i.getValue().name()));
        }
        JSON.add("notify_leaving", NOTIFY_LEAVING);
        final JsonObject COLOR = new JsonObject();
        for (Entry<String, Integer> i : this.color.entrySet()) {
            COLOR.add(i.getKey(), new JsonPrimitive(i.getValue()));
        }
        JSON.add("color", COLOR);
        return new GsonBuilder().serializeNulls().setPrettyPrinting().create()
               .toJson(JSON);
    }

    /**
     * New an area.
     * @param areaName The name of the area to new.
     * @return The previous area holds this name. If no, returns {@code null}.
     * @author REGE
     * @since 0.0.1-a1
     */
    @NoMc
    public Area newArea(String areaName) {
        return this.areas.put(areaName, Area.EMPTY_AREA);
    }

    /**
     * Get an area.
     * @param areaName The name of the area to get.
     * @return The area holds this name, if it is absent, returns {@code null}.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    @NoMc
    public Area getArea(String areaName) {
        return this.areas.get(areaName);
    }

    /**
     * Replace an area.
     * @param areaName The name of the area to be replaced.
     * @param area The area to replace.
     * @return The previous area holds this name. If no, returns {@code null}.
     * @author REGE
     * @since 0.0.1-a1
     */
    @NoMc
    public Area setArea(String areaName, @NotNull Area area) {
        return this.areas.put(areaName, area);
    }

    /**
     * Delete an area.
     * @param areaName The name of the area to delete.
     * @return The area holds this name, if it is absent, returns {@code null}.
     * @author REGE
     * @since 0.0.1-a1
     */
    @NoMc
    public Area deleteArea(String areaName) {
        return this.areas.remove(areaName);
    }

    /**
     * Delete an area.
     * @param areaName The name of the area to rename.
     * @param newAreaName The new name of the area.
     * @return A boolean reflects whether the renaming is succeeded.
     * @author REGE
     * @since 0.0.1-a3
     */
    @NoMc
    public boolean renameArea(String areaName, String newAreaName) {
        if (this.has(newAreaName) || !this.has(areaName)) {
            return false;
        }
        this.setArea(newAreaName, this.getArea(areaName));
        this.deleteArea(areaName);
        return true;
    }

    // Authors: REGE
    // Since: 0.0.1-a6
    public MutableText getColoredAreaNameText(String areaName) {
        final Integer AREA_COLOR = this.getColor(areaName);
        return (AREA_COLOR != null) ?
               Text.literal(areaName)
               .setStyle(Style.EMPTY.withColor(AREA_COLOR.intValue())) :
               Text.literal(areaName);
    }

    // Authors: REGE
    // Since: 0.0.1-a1
    public void sendEnterMessages(Set<String> new_, MinecraftServer server,
                                  ServerPlayerEntity entity) {
        final MutableText ENTERED = Text.empty();
        boolean has_entereds = false;
        for (String i : new_) {
            final NotifyMode MODE = this.getNotifyEnteringMode(i);
            if (MODE == NotifyMode.ALL) {
                LOGGER.info(Text.translatableWithFallback(
                    "areas.enter", "%s entered area %s", entity.getName(), i
                ).getString());
                for (ServerPlayerEntity j :
                     server.getPlayerManager().getPlayerList()) {
                    j.sendMessageToClient(Text.translatableWithFallback(
                        "areas.enter", "%s entered area %s", entity.getName(),
                        this.getColoredAreaNameText(i)
                    ), false);
                }
            } else if (MODE == NotifyMode.CONSOLE) {
                LOGGER.info(Text.translatableWithFallback(
                    "areas.enter", "%s entered area %s", entity.getName(), i
                ).getString());
            } else if (MODE == NotifyMode.TRIGGER) {
                if (has_entereds) {
                    ENTERED
                    .append(Text
                            .translatableWithFallback("areas.enter.separator",
                                                      ", "));
                }
                ENTERED.append(this.getColoredAreaNameText(i));
                has_entereds = true;
            }
        }
        if (has_entereds) {
            entity.networkHandler.sendPacket(new OverlayMessageS2CPacket(
                Text.translatableWithFallback(
                    "areas.enter.you", "You entered area(s) %s", ENTERED
                )
            ));
        }
    }

    // Authors: REGE
    // Since: 0.0.1-a1
    public void sendEnterLeaveMessages(Set<String> old, Set<String> new_,
                                       MinecraftServer server,
                                       ServerPlayerEntity entity) {
        final MutableText ENTERED = Text.empty();
        boolean has_entereds = false;
        final MutableText LEFT = Text.empty();
        boolean has_lefts = false;
        for (String i : old) {
            if (!new_.contains(i) && this.isNotifyLeaving(i)) {
                final NotifyMode MODE = this.getNotifyLeavingMode(i);
                if (MODE == NotifyMode.ALL) {
                    LOGGER.info(Text.translatableWithFallback(
                        "areas.leave", "%s left area %s", entity.getName(), i
                    ).getString());
                    for (ServerPlayerEntity j :
                        server.getPlayerManager().getPlayerList()) {
                        j.sendMessageToClient(Text.translatableWithFallback(
                            "areas.leave", "%s left area %s",entity.getName(),
                            this.getColoredAreaNameText(i)
                        ), false);
                    }
                } else if (MODE == NotifyMode.CONSOLE) {
                    LOGGER.info(Text.translatableWithFallback(
                        "areas.leave", "%s left area %s", entity.getName(), i
                    ).getString());
                } else if (MODE == NotifyMode.TRIGGER) {
                    if (has_lefts) {
                        LEFT.append(Text.translatableWithFallback(
                            "areas.leave.separator", ", "
                        ));
                    }
                    LEFT.append(this.getColoredAreaNameText(i));
                    has_lefts = true;
                }
            }
        }
        for (String i : new_) {
            if (!old.contains(i) && this.isNotifyEntering(i)) {
                final NotifyMode MODE = this.getNotifyEnteringMode(i);
                if (MODE == NotifyMode.ALL) {
                    LOGGER.info(Text.translatableWithFallback(
                        "areas.enter", "%s entered area %s", entity.getName(),i
                    ).getString());
                    for (ServerPlayerEntity j :
                        server.getPlayerManager().getPlayerList()) {
                        j.sendMessageToClient(Text.translatableWithFallback(
                            "areas.enter", "%s entered area %s",
                            entity.getName(), this.getColoredAreaNameText(i)
                        ), false);
                    }
                } else if (MODE == NotifyMode.CONSOLE) {
                    LOGGER.info(Text.translatableWithFallback(
                        "areas.enter", "%s entered area %s", entity.getName(),i
                    ).getString());
                } else if (MODE == NotifyMode.TRIGGER) {
                    if (has_entereds) {
                        ENTERED.append(Text.translatableWithFallback(
                            "areas.enter.separator", ", "
                        ));
                    }
                    ENTERED.append(this.getColoredAreaNameText(i));
                    has_entereds = true;
                }
            }
        }
        if (has_entereds) {
            if (has_lefts) {
                entity.networkHandler.sendPacket(new OverlayMessageS2CPacket(
                    Text.translatableWithFallback(
                        "areas.enter_leave.you",
                        "You entered area(s) %s and left area(s) %s",
                        ENTERED, LEFT
                    )
                ));
            } else {
                entity.networkHandler.sendPacket(new OverlayMessageS2CPacket(
                    Text.translatableWithFallback(
                        "areas.enter.you", "You entered area(s) %s", ENTERED
                    )
                ));
            }
        } else if (has_lefts) {
            entity.networkHandler.sendPacket(new OverlayMessageS2CPacket(
                Text.translatableWithFallback(
                    "areas.leave.you", "You left area(s) %s", LEFT
                )
            ));
        }
    }

    // Authors: REGE
    // Since: 0.0.1-a1
    public void updatePlayerTracking(MinecraftServer server) {
        final List<ServerPlayerEntity> PLAYERLIST =
        server.getPlayerManager().getPlayerList();
        final List<Entry<PlayerEntity, Set<String>>> NEW_PLAYER_TRACKING =
        new ArrayList<>();
        final List<Entry<PlayerEntity, Set<String>>> OLD_PLAYER_TRACKING =
        this.playerTracking;
        for (ServerPlayerEntity i : PLAYERLIST) {
            boolean found = false;
            for (Entry<PlayerEntity, Set<String>> j : OLD_PLAYER_TRACKING) {
                if (i == j.getKey()) {
                    final Set<String> IN_AREAS = new HashSet<>();
                    NEW_PLAYER_TRACKING
                    .add(new SimpleImmutableEntry<>(i, IN_AREAS));
                    final Vec3d POS = i.getPos();
                    final String DIM = getDimensionId(i.getWorld());
                    for (Entry<String, Area> k : this.areas.entrySet()) {
                        if (k.getValue().inArea(POS.x, POS.y, POS.z, DIM)) {
                            IN_AREAS.add(k.getKey());
                        }
                    }
                    this
                    .sendEnterLeaveMessages(j.getValue(), IN_AREAS, server, i);
                    found = true;
                    break;
                }
            }
            if (!found) {
                final Set<String> IN_AREAS = new HashSet<>();
                NEW_PLAYER_TRACKING
                .add(new SimpleImmutableEntry<>(i, IN_AREAS));
                final Vec3d POS = i.getPos();
                final String DIM = getDimensionId(i.getWorld());
                for (Entry<String, Area> k : this.areas.entrySet()) {
                    if (k.getValue().inArea(POS.x, POS.y, POS.z, DIM)) {
                        IN_AREAS.add(k.getKey());
                    }
                }
                this.sendEnterMessages(IN_AREAS, server, i);
            }
        }
        this.playerTracking = NEW_PLAYER_TRACKING;
    }

    /**
     * Import areas to {@code this} in-place.
     * @param json @see {@link com.google.gson.JsonObject}
     * @throws IllegalArgumentException @see
     * {@link #readAreaFromJson(JsonElement)}
     * @author REGE
     * @since 0.0.1-a1
     */
    @NoMc
    public void importAreasFromJson(JsonObject json)
    throws IllegalArgumentException {
        for (String areaName : json.keySet()) {
            this.areas.put(areaName, readAreaFromJson(json.get(areaName)));
        }
    }

    /**
     * @param area The area to be tested.
     * @return The complexity of the area.
     * @throws UnsupportedOperationException If the class of the area is
     * unrecognized.
     * @author REGE
     * @since 0.0.1-a1
     */
    @NoMc
    public static int areaComplexity(Area area)
    throws UnsupportedOperationException {
        if (area == Area.EMPTY_AREA) {
            return 0;
        }
        if (area instanceof CuboidArea) {
            return 1;
        }
        if (area instanceof TriangularPrismArea ||area instanceof EllipsoidArea
            || area instanceof EllipticCylinderArea) {
            return 2;
        }
        if (area instanceof TwoAreasCombination) {
            final TwoAreasCombination AREA = (TwoAreasCombination)area;
            return areaComplexity(AREA.area1) + areaComplexity(AREA.area2);
        }
        throw new UnsupportedOperationException(area.getClass().getName());
    }

    /**
     * Serialize an area to JSON.
     * @param area The area to be serialzed.
     * @return @see {@link com.google.gson.JsonObject}
     * @throws UnsupportedOperationException If the class of the area is
     * unrecognized.
     * @author REGE
     * @since 0.0.1-a1
     */
    @NoMc
    public static JsonElement AreaToJson(Area area)
    throws UnsupportedOperationException {
        if (area == Area.EMPTY_AREA) {
            return JsonNull.INSTANCE;
        }
        if (area instanceof CuboidArea) {
            final CuboidArea CUBOID_AREA = (CuboidArea)area;
            final JsonArray ARR = new JsonArray();
            ARR.add(new JsonPrimitive(CUBOID_AREA.sx));
            ARR.add(new JsonPrimitive(CUBOID_AREA.sy));
            ARR.add(new JsonPrimitive(CUBOID_AREA.sz));
            ARR.add(new JsonPrimitive(CUBOID_AREA.ex));
            ARR.add(new JsonPrimitive(CUBOID_AREA.ey));
            ARR.add(new JsonPrimitive(CUBOID_AREA.ez));
            ARR.add(new JsonPrimitive(CUBOID_AREA.dim));
            return ARR;
        }
        if (area instanceof TriangularPrismArea) {
            final TriangularPrismArea PRISM_AREA = (TriangularPrismArea)area;
            final JsonArray ARR = new JsonArray();
            ARR.add(new JsonPrimitive(PRISM_AREA.x1));
            ARR.add(new JsonPrimitive(PRISM_AREA.y1));
            ARR.add(new JsonPrimitive(PRISM_AREA.z1));
            ARR.add(new JsonPrimitive(PRISM_AREA.x2));
            ARR.add(new JsonPrimitive(PRISM_AREA.y2));
            ARR.add(new JsonPrimitive(PRISM_AREA.z2));
            ARR.add(new JsonPrimitive(PRISM_AREA.x3));
            ARR.add(new JsonPrimitive(PRISM_AREA.z3));
            ARR.add(new JsonPrimitive(PRISM_AREA.dim));
            return ARR;
        }
        if (area instanceof EllipsoidArea) {
            final EllipsoidArea ELLIPSOID_AREA = (EllipsoidArea)area;
            final JsonArray ARR = new JsonArray();
            ARR.add(new JsonPrimitive(ELLIPSOID_AREA.centerX));
            ARR.add(new JsonPrimitive(ELLIPSOID_AREA.centerY));
            ARR.add(new JsonPrimitive(ELLIPSOID_AREA.centerZ));
            ARR.add(new JsonPrimitive(ELLIPSOID_AREA.semiXAxisLength));
            ARR.add(new JsonPrimitive(ELLIPSOID_AREA.semiYAxisLength));
            ARR.add(new JsonPrimitive(ELLIPSOID_AREA.semiZAxisLength));
            ARR.add(new JsonPrimitive(ELLIPSOID_AREA.includeFace));
            ARR.add(new JsonPrimitive(ELLIPSOID_AREA.dim));
            return ARR;
        }
        if (area instanceof EllipticCylinderArea) {
            final EllipticCylinderArea CYLINDER_AREA =
            (EllipticCylinderArea)area;
            final JsonArray ARR = new JsonArray();
            ARR.add(new JsonPrimitive(CYLINDER_AREA.bottomX));
            ARR.add(new JsonPrimitive(CYLINDER_AREA.bottomY));
            ARR.add(new JsonPrimitive(CYLINDER_AREA.bottomZ));
            ARR.add(new JsonPrimitive(CYLINDER_AREA.semiXAxisLength));
            ARR.add(new JsonPrimitive(CYLINDER_AREA.semiZAxisLength));
            ARR.add(new JsonPrimitive(CYLINDER_AREA.includeSideFace));
            ARR.add(new JsonPrimitive(CYLINDER_AREA.height));
            ARR.add(new JsonPrimitive(CYLINDER_AREA.dim));
            return ARR;
        }
        if (area instanceof TwoAreasCombination) {
            final TwoAreasCombination AREA = (TwoAreasCombination)area;
            final JsonArray ARR = new JsonArray();
            ARR.add(AreaToJson(AREA.area1));
            ARR.add(Character.valueOf(AREA.combination_opcode));
            ARR.add(AreaToJson(AREA.area2));
            return ARR;
        }
        throw new UnsupportedOperationException(area.getClass().toString());
    }

    /**
     * Unserialize the JSON to an area.
     * @param json The JSON to unserialize. @see
     * {@link com.google.gson.JsonObject}
     * @return The unserialized area. @see {@link rege.rege.misc.areas.Area}
     * @throws IllegalArgumentException If the JSON cannot be unserialized.
     * @author REGE
     * @since 0.0.1-a1
     */
    @NoMc
    public static Area readAreaFromJson(JsonElement json)
    throws IllegalArgumentException {
        if (json.isJsonNull()) {
            return Area.EMPTY_AREA;
        }
        if (json.isJsonArray()) {
            final JsonArray ARR = (JsonArray)json;
            switch (ARR.size()) {
                case 3: {
                    final JsonElement ARR_E1 = ARR.get(1);
                    if (ARR_E1.isJsonPrimitive()) {
                        final String OPCODE = ARR_E1.getAsString();
                        if (OPCODE.length() != 1) {
                            throw new IllegalArgumentException(
                                "opcode must be 1 char long"
                            );
                        }
                        return new TwoAreasCombination(
                            readAreaFromJson(ARR.get(0)),
                            OPCODE.charAt(0),
                            readAreaFromJson(ARR.get(2))
                        );
                    }
                    throw new IllegalArgumentException(
                        "opcode must be a 1-char-long string"
                    );
                }
                case 4: return new CuboidArea(
                    ARR.get(0).getAsInt(), ARR.get(1).getAsInt(),
                    ARR.get(2).getAsInt(), ARR.get(3).getAsString()
                );
                case 7: return new CuboidArea(
                    ARR.get(0).getAsDouble(), ARR.get(1).getAsDouble(),
                    ARR.get(2).getAsDouble(), ARR.get(3).getAsDouble(),
                    ARR.get(4).getAsDouble(), ARR.get(5).getAsDouble(),
                    ARR.get(6).getAsString()
                );
                case 8: {
                    if (ARR.get(6).isJsonPrimitive() &&
                        ARR.get(6).getAsJsonPrimitive().isBoolean()) {
                        return new EllipsoidArea(
                            ARR.get(0).getAsDouble(), ARR.get(1).getAsDouble(),
                            ARR.get(2).getAsDouble(), ARR.get(3).getAsDouble(),
                            ARR.get(4).getAsDouble(), ARR.get(5).getAsDouble(),
                            ARR.get(6).getAsBoolean(), ARR.get(7).getAsString()
                        );
                    }
                    return new EllipticCylinderArea(
                        ARR.get(0).getAsDouble(), ARR.get(1).getAsDouble(),
                        ARR.get(2).getAsDouble(), ARR.get(3).getAsDouble(),
                        ARR.get(6).getAsDouble(), ARR.get(4).getAsDouble(),
                        ARR.get(5).getAsBoolean(), ARR.get(7).getAsString()
                    );
                }
                case 9: return new TriangularPrismArea(
                    ARR.get(0).getAsDouble(), ARR.get(1).getAsDouble(),
                    ARR.get(2).getAsDouble(), ARR.get(3).getAsDouble(),
                    ARR.get(4).getAsDouble(), ARR.get(5).getAsDouble(),
                    ARR.get(6).getAsDouble(), ARR.get(7).getAsDouble(),
                    ARR.get(8).getAsString()
                );
                default: throw new IllegalArgumentException(
                    "must be a 3-length, 4-length, 7-length, 8-length or 9-length array or null"
                );
            }
        }
        throw new IllegalArgumentException(
            "must be a 3-length, 4-length, 7-length, 8-length or 9-length array or null"
        );
    }

    // Authors: REGE
    // Since: 0.0.1-a1
    public static boolean addAreasRecord(MinecraftServer server, Areas areas) {
        for (Entry<MinecraftServer, Areas> i : AREAS_RECORD) {
            if (server == i.getKey()) {
                return false;
            }
        }
        AREAS_RECORD.add(new SimpleImmutableEntry<>(server, areas));
        return true;
    }

    // Authors: REGE
    // Since: 0.0.1-a1
    @Contract(pure = true)
    public static Areas getAreasRecord(MinecraftServer server) {
        for (int i = 0; i < AREAS_RECORD.size(); i++) {
            if (AREAS_RECORD.get(i).getKey() == server) {
                return AREAS_RECORD.get(i).getValue();
            }
        }
        return null;
    }

    // Authors: REGE
    // Since: 0.0.1-a1
    public static boolean deleteAreasRecord(MinecraftServer server) {
        for (int i = 0; i < AREAS_RECORD.size(); i++) {
            if (AREAS_RECORD.get(i).getKey() == server) {
                AREAS_RECORD.remove(i);
                return true;
            }
        }
        return false;
    }
}
