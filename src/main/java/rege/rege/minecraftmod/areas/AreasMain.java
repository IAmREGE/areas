package rege.rege.minecraftmod.areas;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;

import org.jetbrains.annotations.Range;
import org.slf4j.Logger;

import rege.rege.minecraftmod.areas.server.command.AreasCommand;

import static org.slf4j.LoggerFactory.getLogger;

// Authors: REGE
// Since: 0.0.1-a1
public class AreasMain implements ModInitializer {
    // This logger is used to write text to the console and the log file.
    // It is considered best practice to use your mod id as the logger's name.
    // That way, it's clear which mod wrote info, warnings, and errors.
    /**
     * Class logger.
     * @author REGE
     * @since 0.0.1-a1
     */
    public static final Logger LOGGER = getLogger(AreasMain.class);
    /**
     * Iteration number.
     * @author REGE
     * @since 0.0.1-a3
     */
    public static final @Range(from = 1, to = Long.MAX_VALUE) long
    ITERATION_NUMBER = 7L;
    /**
     * The major version number (like "1" in "1.2.3-a4").
     * @author REGE
     * @since 0.0.1-a5
     */
    public static final @Range(from = 0, to = Short.MAX_VALUE) short
    VER_MAJOR = 0;
    /**
     * The minor version number (like "2" in "1.2.3-a4").
     * @author REGE
     * @since 0.0.1-a5
     */
    public static final @Range(from = 0, to = Short.MAX_VALUE) short
    VER_MINOR = 0;
    /**
     * The micro version number (like "3" in "1.2.3-a4").
     * @author REGE
     * @since 0.0.1-a5
     */
    public static final @Range(from = 0, to = Integer.MAX_VALUE) int
    VER_MICRO = 1;
    /**
     * The development state (like "a" in "1.2.3-a4").
     * {@code 'a'}: Alpha
     * {@code 'b'}: Beta
     * {@code 'r'}: Release candidate
     * {@code 'f'}: Final
     * {@code 'p'}: Patch
     * @author REGE
     * @since 0.0.1-a5
     */
    public static final char VER_DEV_STATE = 'a';
    /**
     * The development revision (like "4" in "1.2.3-a4").
     * @author REGE
     * @since 0.0.1-a5
     */
    public static final @Range(from = 0, to = Byte.MAX_VALUE) byte
    VER_DEV_REVISION = 7;
    /**
     * Version number string.
     * @author REGE
     * @since 0.0.1-a3
     */
    public static final String VER_STRING =
    (VER_DEV_REVISION != 0) ?
    String.format("%d.%d.%d-%c%d", VER_MAJOR, VER_MINOR, VER_MICRO,
                  VER_DEV_STATE, VER_DEV_REVISION) :
    String.format("%d.%d.%d", VER_MAJOR, VER_MINOR, VER_MICRO);

    // Authors: REGE
    // Since: 0.0.1-a1
    @Override
    public void onInitialize() {
        // This code runs as soon as Minecraft is in a mod-load-ready state.
        // However, some things (like resources) may still be uninitialized.
        // Proceed with mild caution.
        ServerTickEvents.END_SERVER_TICK.register(server -> {
            final Areas AREAS = Areas.getAreasRecord(server);
            if (AREAS == null) {
                LOGGER.error("error!!! get areas record returns null");
            } else {
                AREAS.updatePlayerTracking(server);
            }
        });
        CommandRegistrationCallback.EVENT.register(
            (dispatcher, registryAccess, environment) -> {
                AreasCommand.register(dispatcher, environment.dedicated);
            }
        );
    }

    static {
        assert VER_MAJOR >= 0;
        assert VER_MINOR >= (short)0;
        assert VER_MICRO >= (short)0;
        assert VER_DEV_STATE == 'a' || VER_DEV_STATE == 'b' ||
               VER_DEV_STATE == 'r' || VER_DEV_STATE == 'f' ||
               VER_DEV_STATE == 'p';
        assert VER_DEV_REVISION >= (byte)0;
    }
}
