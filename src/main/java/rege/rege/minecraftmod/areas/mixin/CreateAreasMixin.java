package rege.rege.minecraftmod.areas.mixin;

import java.io.FileWriter;
import java.io.IOException;
import java.net.Proxy;

import com.mojang.datafixers.DataFixer;

import net.minecraft.resource.ResourcePackManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.SaveLoader;
import net.minecraft.server.WorldGenerationProgressListenerFactory;
import net.minecraft.util.ApiServices;
import net.minecraft.util.WorldSavePath;
import net.minecraft.world.level.storage.LevelStorage.Session;

import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.Mixin;

import rege.rege.minecraftmod.areas.Areas;

import static rege.rege.minecraftmod.areas.AreasMain.LOGGER;

// Authors: REGE
// Since: 0.0.1-a1
@Mixin(net.minecraft.server.MinecraftServer.class)
public abstract class CreateAreasMixin {
    // Authors: REGE
    // Since: 0.0.1-a1
    @Inject(at = @At("RETURN"), method = "<init>")
    private void joinIntoAreasRecord(
        Thread serverThread, Session session,
        ResourcePackManager dataPackManager, SaveLoader saveLoader,
        Proxy proxy, DataFixer dataFixer, ApiServices apiServices,
        WorldGenerationProgressListenerFactory
        worldGenerationProgressListenerFactory, CallbackInfo info
    ) throws IOException {
        final MinecraftServer SERVER = (MinecraftServer)(Object)this;
        final Areas AREAS = new Areas(SERVER);
        if (!Areas.addAreasRecord(SERVER, AREAS)) {
            LOGGER.error("Failed to add areas record!!!");
        }
    }

    // Authors: REGE
    // Since: 0.0.1-a1
    @Inject(at = @At("RETURN"), method = "save")
    private void saveAreasJsonFile(boolean suppressLogs, boolean flush,
                                   boolean force,
                                   CallbackInfoReturnable<Boolean> info) {
        final MinecraftServer SERVER = (MinecraftServer)(Object)this;
        final Areas AREAS = Areas.getAreasRecord(SERVER);
        try {
            final FileWriter FW = new FileWriter(SERVER.getSavePath(
                WorldSavePath.ROOT
            ).resolve("areas.json").toFile());
            FW.write(AREAS.toJSON());
            FW.close();
            if (!suppressLogs) {
                LOGGER.info("Saving areas.json");
            }
        } catch (IOException e) {
            LOGGER.error("Exception closing the level", e);
        }
    }

    // Authors: REGE
    // Since: 0.0.1-a1
    @Inject(at = @At("RETURN"), method = "shutdown")
    private void removeFromAreasRecord(CallbackInfo info) {
        if (!Areas.deleteAreasRecord((MinecraftServer)(Object)this)) {
            LOGGER.error("Failed to remove areas record!!!");
        }
    }
}
