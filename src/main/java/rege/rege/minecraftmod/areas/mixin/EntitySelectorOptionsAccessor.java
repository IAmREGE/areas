package rege.rege.minecraftmod.areas.mixin;

import java.util.function.Predicate;

import org.spongepowered.asm.mixin.gen.Invoker;
import org.spongepowered.asm.mixin.Mixin;

import net.minecraft.command.EntitySelectorOptions.SelectorHandler;
import net.minecraft.command.EntitySelectorReader;
import net.minecraft.text.Text;

// Authors: REGE
// Since: 0.0.1-a4
@Mixin(net.minecraft.command.EntitySelectorOptions.class)
public interface EntitySelectorOptionsAccessor {
    // Authors: REGE
    // Since: 0.0.1-a4
    @Invoker("putOption")
    public static void
    invokePutOption(String id, SelectorHandler handler,
                    Predicate<EntitySelectorReader>condition,Text description){
        throw new AssertionError();
    }
}
