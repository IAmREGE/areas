package rege.rege.minecraftmod.areas.mixin;

import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.Mixin;

import net.minecraft.server.MinecraftServer;
import net.minecraft.text.Text;
import net.minecraft.util.math.Vec3d;

import rege.rege.minecraftmod.areas.Areas;
import rege.rege.misc.areas.Area;

import static rege.rege.minecraftmod.areas.AreasMain.LOGGER;
import static rege.rege.minecraftmod.areas.server.command.AreasCommand
              .getDimensionId;

// Authors: REGE
// Since: 0.0.1-a4
@Mixin(net.minecraft.command.EntitySelectorOptions.class)
public abstract class EntitySelectorOptionsMixin {
    // Authors: REGE
    // Since: 0.0.1-a4
    @Inject(method = "register", at = @At("TAIL"))
    private static void injectRegister(CallbackInfo info) {
        EntitySelectorOptionsAccessor.invokePutOption("area", reader -> {
            reader.getReader().getCursor();
            final boolean NEGATED = reader.readNegationCharacter();
            final String AREA_NAME = reader.getReader().readString();
            reader.setPredicate(entity -> {
                final MinecraftServer SERVER = entity.getServer();
                if (SERVER == null) {
                    return NEGATED;
                }
                final Areas AREAS = Areas.getAreasRecord(SERVER);
                if (AREAS == null) {
                    LOGGER
                    .error("mod internal error: cannot get areas record");
                    return false;
                }
                if (!AREAS.has(AREA_NAME)) {
                    return NEGATED;
                }
                final Area AREA = AREAS.getArea(AREA_NAME);
                final Vec3d POS = entity.getPos();
                return AREA.inArea(
                    POS.x, POS.y, POS.z, getDimensionId(entity.getWorld())
                ) != NEGATED;
            });
        }, reader -> true, Text.translatableWithFallback(
            "argument.entity.options.area.description", "Area"
        ));
    }
}
