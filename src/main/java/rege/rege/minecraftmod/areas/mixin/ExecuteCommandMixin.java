package rege.rege.minecraftmod.areas.mixin;

import java.util.Collections;

import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.Mixin;

import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import com.mojang.brigadier.tree.CommandNode;

import net.minecraft.command.CommandRegistryAccess;
import net.minecraft.command.argument.DimensionArgumentType;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.command.argument.Vec3ArgumentType;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;

import static rege.rege.minecraftmod.areas.server.command.AreasCommand
              .entitiesInArea;
import static rege.rege.minecraftmod.areas.server.command.AreasCommand
              .posInArea;

// Authors: REGE
// Since: 0.0.1-a4
@Mixin(net.minecraft.server.command.ExecuteCommand.class)
public abstract class ExecuteCommandMixin {
    // Authors: REGE
    // Since: 0.0.1-a4
    @Inject(method = "addConditionArguments", at = @At("RETURN"))
    private static void injectAddConditionArguments(
        CommandNode<ServerCommandSource> root,
        LiteralArgumentBuilder<ServerCommandSource> argumentBuilder,
        boolean positive, CommandRegistryAccess commandRegistryAccess,
        CallbackInfoReturnable<ArgumentBuilder<ServerCommandSource, ?>> info
    ) {
        final ArgumentBuilder<ServerCommandSource, ?> ARGBUILDER =
        info.getReturnValue();
        ARGBUILDER
        .then(
            CommandManager.literal("area")
            .then(
                CommandManager
                .argument("area_name", StringArgumentType.string())
                .then(
                    CommandManager.literal("contains")
                    .then(
                        (CommandManager
                         .argument("pos", Vec3ArgumentType.vec3(false))
                        .fork(root, context -> (
                            (posInArea(context, "area_name", "pos", null) !=
                             positive) ? Collections.emptyList() :
                            Collections.singleton(context.getSource())
                        ))).executes(context -> {
                            if (posInArea(context, "area_name", "pos", null) ==
                                positive) {
                                context.getSource()
                                .sendFeedback(() -> Text.translatable(
                                    "commands.execute.conditional.pass"
                                ), false);
                                return 1;
                            }
                            throw new
                                  SimpleCommandExceptionType(Text.translatable(
                                "commands.execute.conditional.fail"
                            )).create();
                        })
                    )
                    .then(
                        CommandManager
                        .argument("dimension",
                                  DimensionArgumentType.dimension())
                        .then(
                            (CommandManager
                             .argument("pos", Vec3ArgumentType.vec3(false))
                            .fork(root, context -> (
                                (posInArea(context, "area_name", "pos",
                                           "dimension") != positive) ?
                                Collections.emptyList() :
                                Collections.singleton(context.getSource())
                            ))).executes(context -> {
                                if (posInArea(context, "area_name", "pos",
                                              "dimension") == positive) {
                                    context.getSource()
                                    .sendFeedback(() -> Text.translatable(
                                        "commands.execute.conditional.pass"
                                    ), false);
                                    return 1;
                                }
                                throw new
                                      SimpleCommandExceptionType(Text
                                                                 .translatable(
                                    "commands.execute.conditional.fail"
                                )).create();
                            })
                        )
                    )
                )
                .then(
                    CommandManager.literal("keeps_all")
                    .then(
                        (CommandManager
                         .argument("entities", EntityArgumentType.entities())
                        .fork(root, context -> (
                            (entitiesInArea(context, "area_name", "entities",
                                            true) != positive) ?
                            Collections.emptyList() :
                            Collections.singleton(context.getSource())
                        ))).executes(context -> {
                            if (entitiesInArea(context, "area_name","entities",
                                               true) == positive) {
                                context.getSource()
                                .sendFeedback(() -> Text.translatable(
                                    "commands.execute.conditional.pass"
                                ), false);
                                return 1;
                            }
                            throw new
                                  SimpleCommandExceptionType(Text.translatable(
                                "commands.execute.conditional.fail"
                            )).create();
                        })
                    )
                )
                .then(
                    CommandManager.literal("keeps_any")
                    .then(
                        (CommandManager
                         .argument("entities", EntityArgumentType.entities())
                        .fork(root, context -> (
                            (entitiesInArea(context, "area_name", "entities",
                                            false) != positive) ?
                            Collections.emptyList() :
                            Collections.singleton(context.getSource())
                        ))).executes(context -> {
                            if (entitiesInArea(context, "area_name","entities",
                                               false) == positive) {
                                context.getSource()
                                .sendFeedback(() -> Text.translatable(
                                    "commands.execute.conditional.pass"
                                ), false);
                                return 1;
                            }
                            throw new
                                  SimpleCommandExceptionType(Text.translatable(
                                "commands.execute.conditional.fail"
                            )).create();
                        })
                    )
                )
            )
        );
    }
}
