package rege.rege.minecraftmod.areas.server.command;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.Dynamic2CommandExceptionType;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;

import net.minecraft.command.argument.BlockPosArgumentType;
import net.minecraft.command.argument.DimensionArgumentType;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.command.argument.Vec2ArgumentType;
import net.minecraft.command.argument.Vec3ArgumentType;
import net.minecraft.entity.Entity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.MutableText;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec2f;
import net.minecraft.util.math.Vec3d;

import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import rege.rege.minecraftmod.NoMc;
import rege.rege.minecraftmod.areas.Areas;
import rege.rege.misc.areas.Area;
import rege.rege.misc.areas.CuboidArea;
import rege.rege.misc.areas.EllipsoidArea;
import rege.rege.misc.areas.EllipticCylinderArea;
import rege.rege.misc.areas.TriangularPrismArea;
import rege.rege.misc.areas.TwoAreasCombination;

import static rege.rege.minecraftmod.areas.AreasMain.ITERATION_NUMBER;
import static rege.rege.minecraftmod.areas.AreasMain.VER_STRING;

// Authors: REGE
// Since: 0.0.1-a1
public class AreasCommand {
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final DynamicCommandExceptionType CREATE_AREA_EXISTS =
    new DynamicCommandExceptionType(areaName -> Text.translatableWithFallback(
        "commands.areas.failed.create",
        "Could not create area %s as its existence", areaName
    ));
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final DynamicCommandExceptionType RENAME_AREA_NOT_FOUND =
    new DynamicCommandExceptionType(areaName -> Text.translatableWithFallback(
        "commands.areas.failed.rename",
        "Could not rename area %s as its absence", areaName
    ));
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final DynamicCommandExceptionType
    RENAME_NEW_AREA_NAME_OCCUPIED =
    new DynamicCommandExceptionType(newAreaName -> Text.translatableWithFallback(
        "commands.areas.failed.rename.occupied",
        "Could not rename area to name %s (it's occupied)", newAreaName
    ));
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final DynamicCommandExceptionType CLONE_AREA_NOT_FOUND =
    new DynamicCommandExceptionType(areaName -> Text.translatableWithFallback(
        "commands.areas.failed.clone",
        "Could not clone area %s as its absence", areaName
    ));
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final DynamicCommandExceptionType
    CLONE_NEW_AREA_NAME_OCCUPIED =
    new DynamicCommandExceptionType(newAreaName -> Text.translatableWithFallback(
        "commands.areas.failed.clone.occupied",
        "Could not clone area to name %s (it's occupied)", newAreaName
    ));
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final DynamicCommandExceptionType DELETE_AREA_NOT_DOUND =
    new DynamicCommandExceptionType(areaName -> Text.translatableWithFallback(
        "commands.areas.failed.delete",
        "Could not delete area %s as its absence",
        areaName
    ));
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final Dynamic2CommandExceptionType
    DELETE_AREA_HIGH_COMPLEXITY = new Dynamic2CommandExceptionType(
        (areaName, complexity) -> Text.translatableWithFallback(
            "commands.areas.failed.delete.rejected",
            "Could not delete area %s as its complexity " +
            "level %s(to avoid accidental deletion)", areaName, complexity
        )
    );
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final DynamicCommandExceptionType MODIFY_AREA_NOT_FOUND =
    new DynamicCommandExceptionType(areaName -> Text.translatableWithFallback(
        "commands.areas.failed.intersect",
        "Could not modify area %s as its absence", areaName
    ));
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final Dynamic2CommandExceptionType
    NOTIFY_ENTERING_MODE_UNCHANGED = new Dynamic2CommandExceptionType(
        (areaName, modeName) -> Text.translatableWithFallback(
            "commands.areas.failed.notify_entering.set",
            "Could not set the notify entering mode of area %s (it is already \"%s\")",
            areaName, modeName
        )
    );
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final Dynamic2CommandExceptionType
    NOTIFY_LEAVING_MODE_UNCHANGED = new Dynamic2CommandExceptionType(
        (areaName, modeName) -> Text.translatableWithFallback(
            "commands.areas.failed.notify_leaving.set",
            "Could not set the notify leaving mode of area %s (it is already \"%s\")",
            areaName, modeName
        )
    );
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final DynamicCommandExceptionType TEST_AREA_NOT_FOUND =
    new DynamicCommandExceptionType(areaName -> Text.translatableWithFallback(
        "commands.areas.failed.test", "Could not test area %s as its absence",
        areaName
    ));
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final SimpleCommandExceptionType COLOR_IS_UNSET =
    new SimpleCommandExceptionType(Text.translatableWithFallback(
        "commands.areas.failed.color", "Color of the area is unset"
    ));
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final SimpleCommandExceptionType COLOR_IS_ALREADY_UNSET =
    new SimpleCommandExceptionType(Text.translatableWithFallback(
        "commands.areas.failed.color.unset",
        "Color of the area is already unset"
    ));
    // Authors: REGE
    // Since: 0.0.1-a5
    public static final DynamicCommandExceptionType COLOR_UNCHANGED =
    new DynamicCommandExceptionType(intrgb -> Text.translatableWithFallback(
        "commands.areas.failed.color.set", "Color of the area is already #%s",
        intrgb
    ));

    // Authors: REGE
    // Since: 0.0.1-a1
    public static void
    register(CommandDispatcher<ServerCommandSource> dispatcher,
             boolean dedicated) {
        dispatcher.register(CommandManager.literal("areas")
        .requires(source -> source.hasPermissionLevel(1) || !dedicated)
        .then(
            CommandManager.literal("version")
            .executes(context -> {
                context.getSource()
                .sendFeedback(() -> Text.literal(VER_STRING), false);
                return (int)ITERATION_NUMBER;
            })
        )
        .then(
            CommandManager.literal("list")
            .executes(context -> {
                final ServerCommandSource SOURCE = context.getSource();
                final MinecraftServer SERVER = SOURCE.getServer();
                final Areas AREAS = Areas.getAreasRecord(SERVER);
                if (AREAS == null) {
                    SOURCE.sendError(Text.literal(
                        "mod internal error: cannot get areas record"
                    ));
                    return 0;
                }
                return executeAreaList(SOURCE, AREAS, AREAS.areaNames());
            })
            .then(
                CommandManager.literal("notify_entering")
                .executes(context -> {
                    final ServerCommandSource SOURCE = context.getSource();
                    final MinecraftServer SERVER = SOURCE.getServer();
                    final Areas AREAS = Areas.getAreasRecord(SERVER);
                    if (AREAS == null) {
                        SOURCE.sendError(Text.literal(
                            "mod internal error: cannot get areas record"
                        ));
                        return 0;
                    }
                    return executeAreaList(SOURCE, AREAS,
                                           AREAS.notifyEnteringAreas());
                })
                .then(
                    CommandManager.literal("all")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        return executeAreaList(
                            SOURCE, AREAS,
                            AREAS.notifyEnteringAreas(Areas.NotifyMode.ALL)
                        );
                    })
                )
                .then(
                    CommandManager.literal("console")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        return executeAreaList(
                            SOURCE, AREAS,
                            AREAS.notifyEnteringAreas(Areas.NotifyMode.CONSOLE)
                        );
                    })
                )
                .then(
                    CommandManager.literal("trigger")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        return executeAreaList(
                            SOURCE, AREAS,
                            AREAS.notifyEnteringAreas(Areas.NotifyMode.TRIGGER)
                        );
                    })
                )
            )
            .then(
                CommandManager.literal("notify_leaving")
                .executes(context -> {
                    final ServerCommandSource SOURCE = context.getSource();
                    final MinecraftServer SERVER = SOURCE.getServer();
                    final Areas AREAS = Areas.getAreasRecord(SERVER);
                    if (AREAS == null) {
                        SOURCE.sendError(Text.literal(
                            "mod internal error: cannot get areas record"
                        ));
                        return 0;
                    }
                    return executeAreaList(SOURCE, AREAS,
                                           AREAS.notifyLeavingAreas());
                })
                .then(
                    CommandManager.literal("all")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        return executeAreaList(
                            SOURCE, AREAS,
                            AREAS.notifyLeavingAreas(Areas.NotifyMode.ALL)
                        );
                    })
                )
                .then(
                    CommandManager.literal("console")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        return executeAreaList(
                            SOURCE, AREAS,
                            AREAS.notifyLeavingAreas(Areas.NotifyMode.CONSOLE)
                        );
                    })
                )
                .then(
                    CommandManager.literal("trigger")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        return executeAreaList(
                            SOURCE, AREAS,
                            AREAS.notifyLeavingAreas(Areas.NotifyMode.TRIGGER)
                        );
                    })
                )
            )
            .then(
                CommandManager.literal("entities_in")
                .then(
                    CommandManager
                    .argument("entities", EntityArgumentType.entities())
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        final Set<String> AREANAMES = new HashSet<>();
                        for (Entity i : EntityArgumentType
                                        .getEntities(context,"entities")) {
                            final Vec3d POS = i.getPos();
                            final String DIM = getDimensionId(i.getWorld());
                            for (String j : AREAS.areaNames()) {
                                if (AREAS.getArea(j)
                                    .inArea(POS.x, POS.y, POS.z, DIM)) {
                                    AREANAMES.add(j);
                                }
                            }
                        }
                        return executeAreaList(SOURCE, AREAS, AREANAMES);
                    })
                )
            )
            .then(
                CommandManager.literal("entities_out_of")
                .then(
                    CommandManager
                    .argument("entities", EntityArgumentType.entities())
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        final Set<String> AREANAMES = new HashSet<>();
                        for (Entity i : EntityArgumentType
                                        .getEntities(context,"entities")) {
                            final Vec3d POS = i.getPos();
                            final String DIM = getDimensionId(i.getWorld());
                            for (String j : AREAS.areaNames()) {
                                if (!AREAS.getArea(j)
                                     .inArea(POS.x, POS.y, POS.z, DIM)) {
                                    AREANAMES.add(j);
                                }
                            }
                        }
                        return executeAreaList(SOURCE, AREAS, AREANAMES);
                    })
                )
            )
            .then(
                CommandManager.literal("pos_belongs_to")
                .executes(context -> {
                    final ServerCommandSource SOURCE = context.getSource();
                    final MinecraftServer SERVER = SOURCE.getServer();
                    final Areas AREAS = Areas.getAreasRecord(SERVER);
                    if (AREAS == null) {
                        SOURCE.sendError(Text.literal(
                            "mod internal error: cannot get areas record"
                        ));
                        return 0;
                    }
                    final Set<String> AREANAMES = new HashSet<>();
                    final Vec3d POS = SOURCE.getPosition();
                    final String DIM = getDimensionId(SOURCE.getWorld());
                    for (String i : AREAS.areaNames()) {
                        if (AREAS.getArea(i)
                            .inArea(POS.x, POS.y, POS.z, DIM)) {
                            AREANAMES.add(i);
                        }
                    }
                    return executeAreaList(SOURCE, AREAS, AREANAMES);
                })
                .then(
                    CommandManager.argument("pos",Vec3ArgumentType.vec3(false))
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        final Set<String> AREANAMES = new HashSet<>();
                        final Vec3d POS =
                        Vec3ArgumentType.getVec3(context, "pos");
                        final String DIM = getDimensionId(SOURCE.getWorld());
                        for (String i : AREAS.areaNames()) {
                            if (AREAS.getArea(i)
                                .inArea(POS.x, POS.y, POS.z, DIM)) {
                                AREANAMES.add(i);
                            }
                        }
                        return executeAreaList(SOURCE, AREAS, AREANAMES);
                    })
                    .then(
                        CommandManager
                        .argument("dimension",
                                  DimensionArgumentType.dimension())
                        .executes(context -> {
                            final ServerCommandSource SOURCE =
                            context.getSource();
                            final MinecraftServer SERVER = SOURCE.getServer();
                            final Areas AREAS = Areas.getAreasRecord(SERVER);
                            if (AREAS == null) {
                                SOURCE.sendError(Text.literal(
                                    "mod internal error: cannot get areas record"
                                ));
                                return 0;
                            }
                            final Set<String> AREANAMES = new HashSet<>();
                            final Vec3d POS =
                            Vec3ArgumentType.getVec3(context, "pos");
                            final String DIM = getDimensionId(
                                DimensionArgumentType
                                .getDimensionArgument(context, "dimension")
                            );
                            for (String i : AREAS.areaNames()) {
                                if (AREAS.getArea(i)
                                    .inArea(POS.x, POS.y, POS.z, DIM)) {
                                    AREANAMES.add(i);
                                }
                            }
                            return executeAreaList(SOURCE, AREAS, AREANAMES);
                        })
                    )
                )
            )
            .then(
                CommandManager.literal("pos_not_belong_to")
                .executes(context -> {
                    final ServerCommandSource SOURCE = context.getSource();
                    final MinecraftServer SERVER = SOURCE.getServer();
                    final Areas AREAS = Areas.getAreasRecord(SERVER);
                    if (AREAS == null) {
                        SOURCE.sendError(Text.literal(
                            "mod internal error: cannot get areas record"
                        ));
                        return 0;
                    }
                    final Set<String> AREANAMES = new HashSet<>();
                    final Vec3d POS = SOURCE.getPosition();
                    final String DIM = getDimensionId(SOURCE.getWorld());
                    for (String i : AREAS.areaNames()) {
                        if (!AREAS.getArea(i)
                             .inArea(POS.x, POS.y, POS.z, DIM)) {
                            AREANAMES.add(i);
                        }
                    }
                    return executeAreaList(SOURCE, AREAS, AREANAMES);
                })
                .then(
                    CommandManager.argument("pos",Vec3ArgumentType.vec3(false))
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        final Set<String> AREANAMES = new HashSet<>();
                        final Vec3d POS =
                        Vec3ArgumentType.getVec3(context, "pos");
                        final String DIM = getDimensionId(SOURCE.getWorld());
                        for (String i : AREAS.areaNames()) {
                            if (!AREAS.getArea(i)
                                 .inArea(POS.x, POS.y, POS.z, DIM)) {
                                AREANAMES.add(i);
                            }
                        }
                        return executeAreaList(SOURCE, AREAS, AREANAMES);
                    })
                    .then(
                        CommandManager
                        .argument("dimension",
                                  DimensionArgumentType.dimension())
                        .executes(context -> {
                            final ServerCommandSource SOURCE =
                            context.getSource();
                            final MinecraftServer SERVER = SOURCE.getServer();
                            final Areas AREAS = Areas.getAreasRecord(SERVER);
                            if (AREAS == null) {
                                SOURCE.sendError(Text.literal(
                                    "mod internal error: cannot get areas record"
                                ));
                                return 0;
                            }
                            final Set<String> AREANAMES = new HashSet<>();
                            final Vec3d POS =
                            Vec3ArgumentType.getVec3(context, "pos");
                            final String DIM = getDimensionId(
                                DimensionArgumentType
                                .getDimensionArgument(context, "dimension")
                            );
                            for (String i : AREAS.areaNames()) {
                                if (!AREAS.getArea(i)
                                     .inArea(POS.x, POS.y, POS.z, DIM)) {
                                    AREANAMES.add(i);
                                }
                            }
                            return executeAreaList(SOURCE, AREAS, AREANAMES);
                        })
                    )
                )
            )
        )
        .then(
            CommandManager.literal("create")
            .then(
                CommandManager
                .argument("area_name", StringArgumentType.string())
                .executes(context -> {
                    final ServerCommandSource SOURCE = context.getSource();
                    final MinecraftServer SERVER = SOURCE.getServer();
                    final Areas AREAS = Areas.getAreasRecord(SERVER);
                    final String AREA_NAME =
                    StringArgumentType.getString(context, "area_name");
                    if (AREAS == null) {
                        SOURCE.sendError(Text.literal(
                            "mod internal error: cannot get areas record"
                        ));
                        return 0;
                    } else if (AREAS.has(AREA_NAME)) {
                        throw CREATE_AREA_EXISTS.create(AREA_NAME);
                    } else {
                        AREAS.newArea(AREA_NAME);
                        SOURCE.sendFeedback(() ->Text.translatableWithFallback(
                            "commands.areas.success.create",
                            "Successfully created the area"
                        ), true);
                        return 1;
                    }
                })
            )
        )
        .then(
            CommandManager.literal("rename")
            .then(
                CommandManager.argument("area_name",StringArgumentType.string())
                .then(
                    CommandManager
                    .argument("new_area_name", StringArgumentType.string())
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        final String AREA_NAME =
                        StringArgumentType.getString(context, "area_name");
                        final String NEW_AREA_NAME =
                        StringArgumentType.getString(context, "new_area_name");
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        } else if (!AREAS.has(AREA_NAME)) {
                            throw RENAME_AREA_NOT_FOUND.create(AREA_NAME);
                        } else if (AREAS.has(NEW_AREA_NAME)) {
                            throw RENAME_NEW_AREA_NAME_OCCUPIED
                                  .create(NEW_AREA_NAME);
                        } else {
                            AREAS.renameArea(AREA_NAME, NEW_AREA_NAME);
                            SOURCE
                            .sendFeedback(() -> Text.translatableWithFallback(
                                "commands.areas.success.rename",
                                "Successfully renamed the area"
                            ), true);
                            return 1;
                        }
                    })
                )
            )
        )
        .then(
            CommandManager.literal("clone")
            .then(
                CommandManager.argument("area_name",StringArgumentType.string())
                .then(
                    CommandManager
                    .argument("new_area_name", StringArgumentType.string())
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        final String AREA_NAME =
                        StringArgumentType.getString(context, "area_name");
                        final String NEW_AREA_NAME =
                        StringArgumentType.getString(context, "new_area_name");
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        } else if (!AREAS.has(AREA_NAME)) {
                            throw CLONE_AREA_NOT_FOUND.create(AREA_NAME);
                        } else if (AREAS.has(NEW_AREA_NAME)) {
                            throw CLONE_NEW_AREA_NAME_OCCUPIED
                                  .create(NEW_AREA_NAME);
                        } else {
                            AREAS
                            .setArea(NEW_AREA_NAME, AREAS.getArea(AREA_NAME));
                            SOURCE
                            .sendFeedback(() -> Text.translatableWithFallback(
                                "commands.areas.success.clone",
                                "Successfully cloned the area"
                            ), true);
                            return 1;
                        }
                    })
                )
            )
        )
        .then(
            CommandManager.literal("delete")
            .then(
                CommandManager.argument("area_name",StringArgumentType.string())
                .executes(context -> {
                    final ServerCommandSource SOURCE = context.getSource();
                    final MinecraftServer SERVER = SOURCE.getServer();
                    final Areas AREAS = Areas.getAreasRecord(SERVER);
                    final String AREA_NAME =
                    StringArgumentType.getString(context, "area_name");
                    if (AREAS == null) {
                        SOURCE.sendError(Text.literal(
                            "mod internal error: cannot get areas record"
                        ));
                        return 0;
                    } else if (!AREAS.has(AREA_NAME)) {
                        throw DELETE_AREA_NOT_DOUND.create(AREA_NAME);
                    } else {
                        final int COMPLEXITY =
                        Areas.areaComplexity(AREAS.getArea(AREA_NAME));
                        if (COMPLEXITY > 4) {
                            throw DELETE_AREA_HIGH_COMPLEXITY
                                  .create(AREA_NAME, COMPLEXITY);
                        }
                        AREAS.deleteArea(AREA_NAME);
                        SOURCE.sendFeedback(() ->Text.translatableWithFallback(
                            "commands.areas.success.delete",
                            "Successfully deleted the area"
                        ), true);
                        return 1;
                    }
                })
            )
        )
        .then(
            CommandManager.literal("intersect")
            .then(
                CommandManager.argument("area_name",StringArgumentType.string())
                .then(
                    CommandManager.literal("cuboid")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final Vec3d EXEC_POS = SOURCE.getPosition();
                        return executeAreaModify(
                            context, 'i',
                            StringArgumentType.getString(context, "area_name"),
                            new CuboidArea((int)Math.floor(EXEC_POS.x),
                                           (int)Math.floor(EXEC_POS.y),
                                           (int)Math.floor(EXEC_POS.z),
                                           getDimensionId(SOURCE.getWorld())),
                            false
                        );
                    })
                    .then(
                        CommandManager
                        .argument("blockpos", BlockPosArgumentType.blockPos())
                        .executes(context -> {
                            final ServerCommandSource SOURCE =
                            context.getSource();
                            final BlockPos EXEC_POS =
                            BlockPosArgumentType
                            .getBlockPos(context, "blockpos");
                            return executeAreaModify(
                                context, 'i',
                                StringArgumentType
                                .getString(context, "area_name"),
                                new CuboidArea(
                                    EXEC_POS.getX(), EXEC_POS.getY(),
                                    EXEC_POS.getZ(),
                                    getDimensionId(SOURCE.getWorld())
                                ), false
                            );
                        })
                        .then(
                            CommandManager
                            .argument("dimension",
                                      DimensionArgumentType.dimension())
                            .executes(context -> {
                                final BlockPos EXEC_POS =
                                BlockPosArgumentType
                                .getBlockPos(context, "blockpos");
                                return executeAreaModify(
                                    context, 'i',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new CuboidArea(
                                        EXEC_POS.getX(), EXEC_POS.getY(),
                                        EXEC_POS.getZ(), getDimensionId(
                                            DimensionArgumentType
                                            .getDimensionArgument(
                                                context, "dimension"
                                            )
                                        )
                                    ), false
                                );
                            })
                        )
                    )
                    .then(
                        CommandManager
                        .argument("pos1", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("pos2", Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d POS1 =
                                Vec3ArgumentType.getVec3(context, "pos1");
                                final Vec3d POS2 =
                                Vec3ArgumentType.getVec3(context, "pos2");
                                return executeAreaModify(
                                    context, 'i',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new CuboidArea(
                                        POS1.x, POS1.y, POS1.z, POS2.x, POS2.y,
                                        POS2.z,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("dimension",
                                          DimensionArgumentType.dimension())
                                .executes(context -> {
                                    final Vec3d POS1 =
                                    Vec3ArgumentType.getVec3(context, "pos1");
                                    final Vec3d POS2 =
                                    Vec3ArgumentType.getVec3(context, "pos2");
                                    return executeAreaModify(
                                        context, 'i',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new CuboidArea(
                                            POS1.x, POS1.y, POS1.z, POS2.x,
                                            POS2.y, POS2.z, getDimensionId(
                                                DimensionArgumentType
                                                .getDimensionArgument(
                                                    context, "dimension"
                                                )
                                            )
                                        ), false
                                    );
                                })
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("prism")
                    .then(
                        CommandManager
                        .argument("pos1", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("pos2", Vec3ArgumentType.vec3(false))
                            .then(
                                CommandManager
                                .argument("pos3", Vec2ArgumentType.vec2(false))
                                .executes(context -> {
                                    final Vec3d POS1 =
                                    Vec3ArgumentType.getVec3(context, "pos1");
                                    final Vec3d POS2 =
                                    Vec3ArgumentType.getVec3(context, "pos2");
                                    final Vec2f POS3 =
                                    Vec2ArgumentType.getVec2(context, "pos3");
                                    return executeAreaModify(
                                        context, 'i',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new TriangularPrismArea(
                                            POS1.x, POS1.y, POS1.z,
                                            POS2.x, POS2.y, POS2.z,
                                            POS3.x, POS3.y, getDimensionId(
                                                context.getSource().getWorld()
                                            )
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",
                                              DimensionArgumentType
                                              .dimension())
                                    .executes(context -> {
                                        final Vec3d POS1 =
                                        Vec3ArgumentType
                                        .getVec3(context, "pos1");
                                        final Vec3d POS2 =
                                        Vec3ArgumentType
                                        .getVec3(context, "pos2");
                                        final Vec2f POS3 =
                                        Vec2ArgumentType
                                        .getVec2(context, "pos3");
                                        return executeAreaModify(
                                            context, 'i',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new TriangularPrismArea(
                                                POS1.x, POS1.y, POS1.z,
                                                POS2.x, POS2.y, POS2.z,
                                                POS3.x, POS3.y, getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("ellipsoid")
                    .then(
                        CommandManager
                        .argument("center_pos", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("axes_lengths",
                                      Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d CENTER_POS =
                                Vec3ArgumentType.getVec3(context,"center_pos");
                                final Vec3d AXES_LENGTHS =
                                Vec3ArgumentType
                                .getVec3(context, "axes_lengths");
                                return executeAreaModify(
                                    context, 'i',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new EllipsoidArea(
                                        CENTER_POS.x,CENTER_POS.y,CENTER_POS.z,
                                        AXES_LENGTHS.x, AXES_LENGTHS.y,
                                        AXES_LENGTHS.z, true,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("include_face",
                                          BoolArgumentType.bool())
                                .executes(context -> {
                                    final ServerCommandSource SOURCE =
                                    context.getSource();
                                    final Vec3d CENTER_POS =
                                    Vec3ArgumentType
                                    .getVec3(context, "center_pos");
                                    final Vec3d AXES_LENGTHS =
                                    Vec3ArgumentType
                                    .getVec3(context, "axes_lengths");
                                    return executeAreaModify(
                                        context, 'i',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new EllipsoidArea(
                                            CENTER_POS.x, CENTER_POS.y,
                                            CENTER_POS.z, AXES_LENGTHS.x,
                                            AXES_LENGTHS.y, AXES_LENGTHS.z,
                                            BoolArgumentType
                                            .getBool(context, "include_face"),
                                            getDimensionId(SOURCE.getWorld())
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",DimensionArgumentType
                                                          .dimension())
                                    .executes(context -> {
                                        final Vec3d CENTER_POS =
                                        Vec3ArgumentType
                                        .getVec3(context, "center_pos");
                                        final Vec3d AXES_LENGTHS =
                                        Vec3ArgumentType
                                        .getVec3(context, "axes_lengths");
                                        return executeAreaModify(
                                            context, 'i',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new EllipsoidArea(
                                                CENTER_POS.x, CENTER_POS.y,
                                                CENTER_POS.z, AXES_LENGTHS.x,
                                                AXES_LENGTHS.y, AXES_LENGTHS.z,
                                                BoolArgumentType
                                                .getBool(context,
                                                         "include_face"),
                                                getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("cylinder")
                    .then(
                        CommandManager
                        .argument("bottom_pos", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("axes_lengths",
                                      Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d BOTTOM_POS =
                                Vec3ArgumentType.getVec3(context,"bottom_pos");
                                final Vec3d AXES_LENGTHS =
                                Vec3ArgumentType
                                .getVec3(context, "axes_lengths");
                                return executeAreaModify(
                                    context, 'i',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new EllipticCylinderArea(
                                        BOTTOM_POS.x,BOTTOM_POS.y,BOTTOM_POS.z,
                                        AXES_LENGTHS.x, AXES_LENGTHS.y,
                                        AXES_LENGTHS.z, true,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("include_side_face",
                                          BoolArgumentType.bool())
                                .executes(context -> {
                                    final ServerCommandSource SOURCE =
                                    context.getSource();
                                    final Vec3d BOTTOM_POS =
                                    Vec3ArgumentType
                                    .getVec3(context, "bottom_pos");
                                    final Vec3d AXES_LENGTHS =
                                    Vec3ArgumentType
                                    .getVec3(context, "axes_lengths");
                                    return executeAreaModify(
                                        context, 'i',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new EllipticCylinderArea(
                                            BOTTOM_POS.x, BOTTOM_POS.y,
                                            BOTTOM_POS.z, AXES_LENGTHS.x,
                                            AXES_LENGTHS.y, AXES_LENGTHS.z,
                                            BoolArgumentType
                                            .getBool(context,
                                                     "include_side_face"),
                                            getDimensionId(SOURCE.getWorld())
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",DimensionArgumentType
                                                          .dimension())
                                    .executes(context -> {
                                        final Vec3d BOTTOM_POS =
                                        Vec3ArgumentType
                                        .getVec3(context, "bottom_pos");
                                        final Vec3d AXES_LENGTHS =
                                        Vec3ArgumentType
                                        .getVec3(context, "axes_lengths");
                                        return executeAreaModify(
                                            context, 'i',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new EllipticCylinderArea(
                                                BOTTOM_POS.x, BOTTOM_POS.y,
                                                BOTTOM_POS.z, AXES_LENGTHS.x,
                                                AXES_LENGTHS.y, AXES_LENGTHS.z,
                                                BoolArgumentType
                                                .getBool(context,
                                                         "include_side_face"),
                                                getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
            )
        )
        .then(
            CommandManager.literal("union")
            .then(
                CommandManager.argument("area_name",StringArgumentType.string())
                .then(
                    CommandManager.literal("cuboid")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final Vec3d EXEC_POS = SOURCE.getPosition();
                        return executeAreaModify(
                            context, 'u',
                            StringArgumentType.getString(context, "area_name"),
                            new CuboidArea((int)Math.floor(EXEC_POS.x),
                                           (int)Math.floor(EXEC_POS.y),
                                           (int)Math.floor(EXEC_POS.z),
                                           getDimensionId(SOURCE.getWorld())),
                            false
                        );
                    })
                    .then(
                        CommandManager
                        .argument("blockpos", BlockPosArgumentType.blockPos())
                        .executes(context -> {
                            final ServerCommandSource SOURCE =
                            context.getSource();
                            final BlockPos EXEC_POS =
                            BlockPosArgumentType
                            .getBlockPos(context, "blockpos");
                            return executeAreaModify(
                                context, 'u',
                                StringArgumentType
                                .getString(context, "area_name"),
                                new CuboidArea(
                                    EXEC_POS.getX(), EXEC_POS.getY(),
                                    EXEC_POS.getZ(),
                                    getDimensionId(SOURCE.getWorld())
                                ), false
                            );
                        })
                        .then(
                            CommandManager
                            .argument("dimension",
                                      DimensionArgumentType.dimension())
                            .executes(context -> {
                                final BlockPos EXEC_POS =
                                BlockPosArgumentType
                                .getBlockPos(context, "blockpos");
                                return executeAreaModify(
                                    context, 'u',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new CuboidArea(
                                        EXEC_POS.getX(), EXEC_POS.getY(),
                                        EXEC_POS.getZ(), getDimensionId(
                                            DimensionArgumentType
                                            .getDimensionArgument(
                                                context, "dimension"
                                            ))
                                        ), false
                                );
                            })
                        )
                    )
                    .then(
                        CommandManager
                        .argument("pos1", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("pos2", Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d POS1 =
                                Vec3ArgumentType.getVec3(context, "pos1");
                                final Vec3d POS2 =
                                Vec3ArgumentType.getVec3(context, "pos2");
                                return executeAreaModify(
                                    context, 'u',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new CuboidArea(
                                        POS1.x, POS1.y, POS1.z, POS2.x, POS2.y,
                                        POS2.z,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("dimension",
                                          DimensionArgumentType.dimension())
                                .executes(context -> {
                                    final Vec3d POS1 =
                                    Vec3ArgumentType.getVec3(context, "pos1");
                                    final Vec3d POS2 =
                                    Vec3ArgumentType.getVec3(context, "pos2");
                                    return executeAreaModify(
                                        context, 'u',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new CuboidArea(
                                            POS1.x, POS1.y, POS1.z, POS2.x,
                                            POS2.y, POS2.z, getDimensionId(
                                                DimensionArgumentType
                                                .getDimensionArgument(
                                                    context, "dimension"
                                                )
                                            )
                                        ), false
                                    );
                                })
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("prism")
                    .then(
                        CommandManager
                        .argument("pos1", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("pos2", Vec3ArgumentType.vec3(false))
                            .then(
                                CommandManager
                                .argument("pos3", Vec2ArgumentType.vec2(false))
                                .executes(context -> {
                                    final Vec3d POS1 =
                                    Vec3ArgumentType.getVec3(context, "pos1");
                                    final Vec3d POS2 =
                                    Vec3ArgumentType.getVec3(context, "pos2");
                                    final Vec2f POS3 =
                                    Vec2ArgumentType.getVec2(context, "pos3");
                                    return executeAreaModify(
                                        context, 'u',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new TriangularPrismArea(
                                            POS1.x, POS1.y, POS1.z,
                                            POS2.x, POS2.y, POS2.z,
                                            POS3.x, POS3.y, getDimensionId(
                                                context.getSource().getWorld()
                                            )
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",
                                              DimensionArgumentType
                                              .dimension())
                                    .executes(context -> {
                                        final Vec3d POS1 =
                                        Vec3ArgumentType
                                        .getVec3(context, "pos1");
                                        final Vec3d POS2 =
                                        Vec3ArgumentType
                                        .getVec3(context, "pos2");
                                        final Vec2f POS3 =
                                        Vec2ArgumentType
                                        .getVec2(context, "pos3");
                                        return executeAreaModify(
                                            context, 'u',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new TriangularPrismArea(
                                                POS1.x, POS1.y, POS1.z,
                                                POS2.x, POS2.y, POS2.z,
                                                POS3.x, POS3.y, getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("ellipsoid")
                    .then(
                        CommandManager
                        .argument("center_pos", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("axes_lengths",
                                      Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d CENTER_POS =
                                Vec3ArgumentType.getVec3(context,"center_pos");
                                final Vec3d AXES_LENGTHS =
                                Vec3ArgumentType
                                .getVec3(context, "axes_lengths");
                                return executeAreaModify(
                                    context, 'u',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new EllipsoidArea(
                                        CENTER_POS.x,CENTER_POS.y,CENTER_POS.z,
                                        AXES_LENGTHS.x, AXES_LENGTHS.y,
                                        AXES_LENGTHS.z, true,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("include_face",
                                          BoolArgumentType.bool())
                                .executes(context -> {
                                    final ServerCommandSource SOURCE =
                                    context.getSource();
                                    final Vec3d CENTER_POS =
                                    Vec3ArgumentType
                                    .getVec3(context, "center_pos");
                                    final Vec3d AXES_LENGTHS =
                                    Vec3ArgumentType
                                    .getVec3(context, "axes_lengths");
                                    return executeAreaModify(
                                        context, 'u',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new EllipsoidArea(
                                            CENTER_POS.x, CENTER_POS.y,
                                            CENTER_POS.z, AXES_LENGTHS.x,
                                            AXES_LENGTHS.y, AXES_LENGTHS.z,
                                            BoolArgumentType
                                            .getBool(context, "include_face"),
                                            getDimensionId(SOURCE.getWorld())
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",DimensionArgumentType
                                                          .dimension())
                                    .executes(context -> {
                                        final Vec3d CENTER_POS =
                                        Vec3ArgumentType
                                        .getVec3(context, "center_pos");
                                        final Vec3d AXES_LENGTHS =
                                        Vec3ArgumentType
                                        .getVec3(context, "axes_lengths");
                                        return executeAreaModify(
                                            context, 'u',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new EllipsoidArea(
                                                CENTER_POS.x, CENTER_POS.y,
                                                CENTER_POS.z, AXES_LENGTHS.x,
                                                AXES_LENGTHS.y, AXES_LENGTHS.z,
                                                BoolArgumentType
                                                .getBool(context,
                                                         "include_face"),
                                                getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("cylinder")
                    .then(
                        CommandManager
                        .argument("bottom_pos", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("axes_lengths",
                                      Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d BOTTOM_POS =
                                Vec3ArgumentType.getVec3(context,"bottom_pos");
                                final Vec3d AXES_LENGTHS =
                                Vec3ArgumentType
                                .getVec3(context, "axes_lengths");
                                return executeAreaModify(
                                    context, 'u',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new EllipticCylinderArea(
                                        BOTTOM_POS.x,BOTTOM_POS.y,BOTTOM_POS.z,
                                        AXES_LENGTHS.x, AXES_LENGTHS.y,
                                        AXES_LENGTHS.z, true,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("include_side_face",
                                          BoolArgumentType.bool())
                                .executes(context -> {
                                    final ServerCommandSource SOURCE =
                                    context.getSource();
                                    final Vec3d BOTTOM_POS =
                                    Vec3ArgumentType
                                    .getVec3(context, "bottom_pos");
                                    final Vec3d AXES_LENGTHS =
                                    Vec3ArgumentType
                                    .getVec3(context, "axes_lengths");
                                    return executeAreaModify(
                                        context, 'u',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new EllipticCylinderArea(
                                            BOTTOM_POS.x, BOTTOM_POS.y,
                                            BOTTOM_POS.z, AXES_LENGTHS.x,
                                            AXES_LENGTHS.y, AXES_LENGTHS.z,
                                            BoolArgumentType
                                            .getBool(context,
                                                     "include_side_face"),
                                            getDimensionId(SOURCE.getWorld())
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",DimensionArgumentType
                                                          .dimension())
                                    .executes(context -> {
                                        final Vec3d BOTTOM_POS =
                                        Vec3ArgumentType
                                        .getVec3(context, "bottom_pos");
                                        final Vec3d AXES_LENGTHS =
                                        Vec3ArgumentType
                                        .getVec3(context, "axes_lengths");
                                        return executeAreaModify(
                                            context, 'u',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new EllipticCylinderArea(
                                                BOTTOM_POS.x, BOTTOM_POS.y,
                                                BOTTOM_POS.z, AXES_LENGTHS.x,
                                                AXES_LENGTHS.y, AXES_LENGTHS.z,
                                                BoolArgumentType
                                                .getBool(context,
                                                         "include_side_face"),
                                                getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
            )
        )
        .then(
            CommandManager.literal("difference")
            .then(
                CommandManager.argument("area_name",StringArgumentType.string())
                .then(
                    CommandManager.literal("cuboid")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final Vec3d EXEC_POS = SOURCE.getPosition();
                        return executeAreaModify(
                            context, 'd',
                            StringArgumentType.getString(context, "area_name"),
                            new CuboidArea((int)Math.floor(EXEC_POS.x),
                                           (int)Math.floor(EXEC_POS.y),
                                           (int)Math.floor(EXEC_POS.z),
                                           getDimensionId(SOURCE.getWorld())),
                            false
                        );
                    })
                    .then(
                        CommandManager
                        .argument("blockpos", BlockPosArgumentType.blockPos())
                        .executes(context -> {
                            final ServerCommandSource SOURCE =
                            context.getSource();
                            final BlockPos EXEC_POS =
                            BlockPosArgumentType
                            .getBlockPos(context, "blockpos");
                            return executeAreaModify(
                                context, 'd',
                                StringArgumentType
                                .getString(context, "area_name"),
                                new CuboidArea(
                                    EXEC_POS.getX(), EXEC_POS.getY(),
                                    EXEC_POS.getZ(),
                                    getDimensionId(SOURCE.getWorld())
                                ), false
                            );
                        })
                        .then(
                            CommandManager
                            .argument("dimension",
                                      DimensionArgumentType.dimension())
                            .executes(context -> {
                                final BlockPos EXEC_POS =
                                BlockPosArgumentType
                                .getBlockPos(context, "blockpos");
                                return executeAreaModify(
                                    context, 'd',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new CuboidArea(
                                        EXEC_POS.getX(), EXEC_POS.getY(),
                                        EXEC_POS.getZ(), getDimensionId(
                                            DimensionArgumentType
                                            .getDimensionArgument(
                                                context, "dimension"
                                            )
                                        )
                                    ), false
                                );
                            })
                        )
                    )
                    .then(
                        CommandManager
                        .argument("pos1", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("pos2", Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d POS1 =
                                Vec3ArgumentType.getVec3(context, "pos1");
                                final Vec3d POS2 =
                                Vec3ArgumentType.getVec3(context, "pos2");
                                return executeAreaModify(
                                    context, 'd',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new CuboidArea(
                                        POS1.x, POS1.y, POS1.z, POS2.x, POS2.y,
                                        POS2.z,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("dimension",
                                          DimensionArgumentType.dimension())
                                .executes(context -> {
                                    final Vec3d POS1 =
                                    Vec3ArgumentType.getVec3(context, "pos1");
                                    final Vec3d POS2 =
                                    Vec3ArgumentType.getVec3(context, "pos2");
                                    return executeAreaModify(
                                        context, 'd',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new CuboidArea(
                                            POS1.x, POS1.y, POS1.z, POS2.x,
                                            POS2.y, POS2.z, getDimensionId(
                                                DimensionArgumentType
                                                .getDimensionArgument(
                                                    context, "dimension"
                                                )
                                            )
                                        ), false
                                    );
                                })
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("prism")
                    .then(
                        CommandManager
                        .argument("pos1", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("pos2", Vec3ArgumentType.vec3(false))
                            .then(
                                CommandManager
                                .argument("pos3", Vec2ArgumentType.vec2(false))
                                .executes(context -> {
                                    final Vec3d POS1 =
                                    Vec3ArgumentType.getVec3(context, "pos1");
                                    final Vec3d POS2 =
                                    Vec3ArgumentType.getVec3(context, "pos2");
                                    final Vec2f POS3 =
                                    Vec2ArgumentType.getVec2(context, "pos3");
                                    return executeAreaModify(
                                        context, 'd',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new TriangularPrismArea(
                                            POS1.x, POS1.y, POS1.z,
                                            POS2.x, POS2.y, POS2.z,
                                            POS3.x, POS3.y, getDimensionId(
                                                context.getSource().getWorld()
                                            )
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",
                                              DimensionArgumentType
                                              .dimension())
                                    .executes(context -> {
                                        final Vec3d POS1 =
                                        Vec3ArgumentType
                                        .getVec3(context, "pos1");
                                        final Vec3d POS2 =
                                        Vec3ArgumentType
                                        .getVec3(context, "pos2");
                                        final Vec2f POS3 =
                                        Vec2ArgumentType
                                        .getVec2(context, "pos3");
                                        return executeAreaModify(
                                            context, 'd',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new TriangularPrismArea(
                                                POS1.x, POS1.y, POS1.z,
                                                POS2.x, POS2.y, POS2.z,
                                                POS3.x, POS3.y, getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("ellipsoid")
                    .then(
                        CommandManager
                        .argument("center_pos", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("axes_lengths",
                                      Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d CENTER_POS =
                                Vec3ArgumentType.getVec3(context,"center_pos");
                                final Vec3d AXES_LENGTHS =
                                Vec3ArgumentType
                                .getVec3(context, "axes_lengths");
                                return executeAreaModify(
                                    context, 'd',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new EllipsoidArea(
                                        CENTER_POS.x,CENTER_POS.y,CENTER_POS.z,
                                        AXES_LENGTHS.x, AXES_LENGTHS.y,
                                        AXES_LENGTHS.z, true,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("include_face",
                                          BoolArgumentType.bool())
                                .executes(context -> {
                                    final ServerCommandSource SOURCE =
                                    context.getSource();
                                    final Vec3d CENTER_POS =
                                    Vec3ArgumentType
                                    .getVec3(context, "center_pos");
                                    final Vec3d AXES_LENGTHS =
                                    Vec3ArgumentType
                                    .getVec3(context, "axes_lengths");
                                    return executeAreaModify(
                                        context, 'd',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new EllipsoidArea(
                                            CENTER_POS.x, CENTER_POS.y,
                                            CENTER_POS.z, AXES_LENGTHS.x,
                                            AXES_LENGTHS.y, AXES_LENGTHS.z,
                                            BoolArgumentType
                                            .getBool(context, "include_face"),
                                            getDimensionId(SOURCE.getWorld())
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",DimensionArgumentType
                                                          .dimension())
                                    .executes(context -> {
                                        final Vec3d CENTER_POS =
                                        Vec3ArgumentType
                                        .getVec3(context, "center_pos");
                                        final Vec3d AXES_LENGTHS =
                                        Vec3ArgumentType
                                        .getVec3(context, "axes_lengths");
                                        return executeAreaModify(
                                            context, 'd',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new EllipsoidArea(
                                                CENTER_POS.x, CENTER_POS.y,
                                                CENTER_POS.z, AXES_LENGTHS.x,
                                                AXES_LENGTHS.y, AXES_LENGTHS.z,
                                                BoolArgumentType
                                                .getBool(context,
                                                         "include_face"),
                                                getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("cylinder")
                    .then(
                        CommandManager
                        .argument("bottom_pos", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("axes_lengths",
                                      Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d BOTTOM_POS =
                                Vec3ArgumentType.getVec3(context,"bottom_pos");
                                final Vec3d AXES_LENGTHS =
                                Vec3ArgumentType
                                .getVec3(context, "axes_lengths");
                                return executeAreaModify(
                                    context, 'd',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new EllipticCylinderArea(
                                        BOTTOM_POS.x,BOTTOM_POS.y,BOTTOM_POS.z,
                                        AXES_LENGTHS.x, AXES_LENGTHS.y,
                                        AXES_LENGTHS.z, true,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("include_side_face",
                                          BoolArgumentType.bool())
                                .executes(context -> {
                                    final ServerCommandSource SOURCE =
                                    context.getSource();
                                    final Vec3d BOTTOM_POS =
                                    Vec3ArgumentType
                                    .getVec3(context, "bottom_pos");
                                    final Vec3d AXES_LENGTHS =
                                    Vec3ArgumentType
                                    .getVec3(context, "axes_lengths");
                                    return executeAreaModify(
                                        context, 'd',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new EllipticCylinderArea(
                                            BOTTOM_POS.x, BOTTOM_POS.y,
                                            BOTTOM_POS.z, AXES_LENGTHS.x,
                                            AXES_LENGTHS.y, AXES_LENGTHS.z,
                                            BoolArgumentType
                                            .getBool(context,
                                                     "include_side_face"),
                                            getDimensionId(SOURCE.getWorld())
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",DimensionArgumentType
                                                          .dimension())
                                    .executes(context -> {
                                        final Vec3d BOTTOM_POS =
                                        Vec3ArgumentType
                                        .getVec3(context, "bottom_pos");
                                        final Vec3d AXES_LENGTHS =
                                        Vec3ArgumentType
                                        .getVec3(context, "axes_lengths");
                                        return executeAreaModify(
                                            context, 'd',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new EllipticCylinderArea(
                                                BOTTOM_POS.x, BOTTOM_POS.y,
                                                BOTTOM_POS.z, AXES_LENGTHS.x,
                                                AXES_LENGTHS.y, AXES_LENGTHS.z,
                                                BoolArgumentType
                                                .getBool(context,
                                                         "include_side_face"),
                                                getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
            )
        )
        .then(
            CommandManager.literal("difference_by")
            .then(
                CommandManager.argument("area_name",StringArgumentType.string())
                .then(
                    CommandManager.literal("cuboid")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final Vec3d EXEC_POS = SOURCE.getPosition();
                        return executeAreaModify(
                            context, 'd',
                            StringArgumentType.getString(context, "area_name"),
                            new CuboidArea((int)Math.floor(EXEC_POS.x),
                                           (int)Math.floor(EXEC_POS.y),
                                           (int)Math.floor(EXEC_POS.z),
                                           getDimensionId(SOURCE.getWorld())),
                            true
                        );
                    })
                    .then(
                        CommandManager
                        .argument("blockpos", BlockPosArgumentType.blockPos())
                        .executes(context -> {
                            final ServerCommandSource SOURCE =
                            context.getSource();
                            final BlockPos EXEC_POS =
                            BlockPosArgumentType
                            .getBlockPos(context, "blockpos");
                            return executeAreaModify(
                                context, 'd',
                                StringArgumentType
                                .getString(context, "area_name"),
                                new CuboidArea(
                                    EXEC_POS.getX(), EXEC_POS.getY(),
                                    EXEC_POS.getZ(),
                                    getDimensionId(SOURCE.getWorld())
                                ), true
                            );
                        })
                        .then(
                            CommandManager
                            .argument("dimension",
                                      DimensionArgumentType.dimension())
                            .executes(context -> {
                                final BlockPos EXEC_POS =
                                BlockPosArgumentType
                                .getBlockPos(context, "blockpos");
                                return executeAreaModify(
                                    context, 'd',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new CuboidArea(
                                        EXEC_POS.getX(), EXEC_POS.getY(),
                                        EXEC_POS.getZ(), getDimensionId(
                                            DimensionArgumentType
                                            .getDimensionArgument(
                                                context, "dimension"
                                            )
                                        )
                                    ), true
                                );
                            })
                        )
                    )
                    .then(
                        CommandManager
                        .argument("pos1", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("pos2", Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d POS1 =
                                Vec3ArgumentType.getVec3(context, "pos1");
                                final Vec3d POS2 =
                                Vec3ArgumentType.getVec3(context, "pos2");
                                return executeAreaModify(
                                    context, 'd',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new CuboidArea(
                                        POS1.x, POS1.y, POS1.z, POS2.x, POS2.y,
                                        POS2.z,
                                        getDimensionId(SOURCE.getWorld())
                                    ),
                                    true
                                );
                            })
                            .then(
                                CommandManager
                                .argument("dimension",
                                          DimensionArgumentType.dimension())
                                .executes(context -> {
                                    final Vec3d POS1 =
                                    Vec3ArgumentType.getVec3(context, "pos1");
                                    final Vec3d POS2 =
                                    Vec3ArgumentType.getVec3(context, "pos2");
                                    return executeAreaModify(
                                        context, 'd',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new CuboidArea(
                                            POS1.x, POS1.y, POS1.z, POS2.x,
                                            POS2.y, POS2.z, getDimensionId(
                                                DimensionArgumentType
                                                .getDimensionArgument(
                                                    context, "dimension"
                                                )
                                            )
                                        ), true
                                    );
                                })
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("prism")
                    .then(
                        CommandManager
                        .argument("pos1", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("pos2", Vec3ArgumentType.vec3(false))
                            .then(
                                CommandManager
                                .argument("pos3", Vec2ArgumentType.vec2(false))
                                .executes(context -> {
                                    final Vec3d POS1 =
                                    Vec3ArgumentType.getVec3(context, "pos1");
                                    final Vec3d POS2 =
                                    Vec3ArgumentType.getVec3(context, "pos2");
                                    final Vec2f POS3 =
                                    Vec2ArgumentType.getVec2(context, "pos3");
                                    return executeAreaModify(
                                        context, 'd',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new TriangularPrismArea(
                                            POS1.x, POS1.y, POS1.z,
                                            POS2.x, POS2.y, POS2.z,
                                            POS3.x, POS3.y, getDimensionId(
                                                context.getSource().getWorld()
                                            )
                                        ), true
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",
                                              DimensionArgumentType
                                              .dimension())
                                    .executes(context -> {
                                        final Vec3d POS1 =
                                        Vec3ArgumentType
                                        .getVec3(context, "pos1");
                                        final Vec3d POS2 =
                                        Vec3ArgumentType
                                        .getVec3(context, "pos2");
                                        final Vec2f POS3 =
                                        Vec2ArgumentType
                                        .getVec2(context, "pos3");
                                        return executeAreaModify(
                                            context, 'd',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new TriangularPrismArea(
                                                POS1.x, POS1.y, POS1.z,
                                                POS2.x, POS2.y, POS2.z,
                                                POS3.x, POS3.y, getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), true
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("ellipsoid")
                    .then(
                        CommandManager
                        .argument("center_pos", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("axes_lengths",
                                      Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d CENTER_POS =
                                Vec3ArgumentType.getVec3(context,"center_pos");
                                final Vec3d AXES_LENGTHS =
                                Vec3ArgumentType
                                .getVec3(context, "axes_lengths");
                                return executeAreaModify(
                                    context, 'd',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new EllipsoidArea(
                                        CENTER_POS.x,CENTER_POS.y,CENTER_POS.z,
                                        AXES_LENGTHS.x, AXES_LENGTHS.y,
                                        AXES_LENGTHS.z, true,
                                        getDimensionId(SOURCE.getWorld())
                                    ), true
                                );
                            })
                            .then(
                                CommandManager
                                .argument("include_face",
                                          BoolArgumentType.bool())
                                .executes(context -> {
                                    final ServerCommandSource SOURCE =
                                    context.getSource();
                                    final Vec3d CENTER_POS =
                                    Vec3ArgumentType
                                    .getVec3(context, "center_pos");
                                    final Vec3d AXES_LENGTHS =
                                    Vec3ArgumentType
                                    .getVec3(context, "axes_lengths");
                                    return executeAreaModify(
                                        context, 'd',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new EllipsoidArea(
                                            CENTER_POS.x, CENTER_POS.y,
                                            CENTER_POS.z, AXES_LENGTHS.x,
                                            AXES_LENGTHS.y, AXES_LENGTHS.z,
                                            BoolArgumentType
                                            .getBool(context, "include_face"),
                                            getDimensionId(SOURCE.getWorld())
                                        ), true
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",DimensionArgumentType
                                                          .dimension())
                                    .executes(context -> {
                                        final Vec3d CENTER_POS =
                                        Vec3ArgumentType
                                        .getVec3(context, "center_pos");
                                        final Vec3d AXES_LENGTHS =
                                        Vec3ArgumentType
                                        .getVec3(context, "axes_lengths");
                                        return executeAreaModify(
                                            context, 'd',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new EllipsoidArea(
                                                CENTER_POS.x, CENTER_POS.y,
                                                CENTER_POS.z, AXES_LENGTHS.x,
                                                AXES_LENGTHS.y, AXES_LENGTHS.z,
                                                BoolArgumentType
                                                .getBool(context,
                                                         "include_face"),
                                                getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), true
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("cylinder")
                    .then(
                        CommandManager
                        .argument("bottom_pos", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("axes_lengths",
                                      Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d BOTTOM_POS =
                                Vec3ArgumentType.getVec3(context,"bottom_pos");
                                final Vec3d AXES_LENGTHS =
                                Vec3ArgumentType
                                .getVec3(context, "axes_lengths");
                                return executeAreaModify(
                                    context, 'd',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new EllipticCylinderArea(
                                        BOTTOM_POS.x,BOTTOM_POS.y,BOTTOM_POS.z,
                                        AXES_LENGTHS.x, AXES_LENGTHS.y,
                                        AXES_LENGTHS.z, true,
                                        getDimensionId(SOURCE.getWorld())
                                    ), true
                                );
                            })
                            .then(
                                CommandManager
                                .argument("include_side_face",
                                          BoolArgumentType.bool())
                                .executes(context -> {
                                    final ServerCommandSource SOURCE =
                                    context.getSource();
                                    final Vec3d BOTTOM_POS =
                                    Vec3ArgumentType
                                    .getVec3(context, "bottom_pos");
                                    final Vec3d AXES_LENGTHS =
                                    Vec3ArgumentType
                                    .getVec3(context, "axes_lengths");
                                    return executeAreaModify(
                                        context, 'd',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new EllipticCylinderArea(
                                            BOTTOM_POS.x, BOTTOM_POS.y,
                                            BOTTOM_POS.z, AXES_LENGTHS.x,
                                            AXES_LENGTHS.y, AXES_LENGTHS.z,
                                            BoolArgumentType
                                            .getBool(context,
                                                     "include_side_face"),
                                            getDimensionId(SOURCE.getWorld())
                                        ), true
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",DimensionArgumentType
                                                          .dimension())
                                    .executes(context -> {
                                        final Vec3d BOTTOM_POS =
                                        Vec3ArgumentType
                                        .getVec3(context, "bottom_pos");
                                        final Vec3d AXES_LENGTHS =
                                        Vec3ArgumentType
                                        .getVec3(context, "axes_lengths");
                                        return executeAreaModify(
                                            context, 'd',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new EllipticCylinderArea(
                                                BOTTOM_POS.x, BOTTOM_POS.y,
                                                BOTTOM_POS.z, AXES_LENGTHS.x,
                                                AXES_LENGTHS.y, AXES_LENGTHS.z,
                                                BoolArgumentType
                                                .getBool(context,
                                                         "include_side_face"),
                                                getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), true
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
            )
        )
        .then(
            CommandManager.literal("sym_difference")
            .then(
                CommandManager.argument("area_name",StringArgumentType.string())
                .then(
                    CommandManager.literal("cuboid")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final Vec3d EXEC_POS = SOURCE.getPosition();
                        return executeAreaModify(
                            context, 's',
                            StringArgumentType.getString(context, "area_name"),
                            new CuboidArea((int)Math.floor(EXEC_POS.x),
                                           (int)Math.floor(EXEC_POS.y),
                                           (int)Math.floor(EXEC_POS.z),
                                           getDimensionId(SOURCE.getWorld())),
                            false
                        );
                    })
                    .then(
                        CommandManager
                        .argument("blockpos", BlockPosArgumentType.blockPos())
                        .executes(context -> {
                            final ServerCommandSource SOURCE =
                            context.getSource();
                            final BlockPos EXEC_POS =
                            BlockPosArgumentType
                            .getBlockPos(context, "blockpos");
                            return executeAreaModify(
                                context, 's',
                                StringArgumentType
                                .getString(context, "area_name"),
                                new CuboidArea(
                                    EXEC_POS.getX(), EXEC_POS.getY(),
                                    EXEC_POS.getZ(),
                                    getDimensionId(SOURCE.getWorld())
                                ), false
                            );
                        })
                        .then(
                            CommandManager
                            .argument("dimension",
                                      DimensionArgumentType.dimension())
                            .executes(context -> {
                                final BlockPos EXEC_POS =
                                BlockPosArgumentType
                                .getBlockPos(context, "blockpos");
                                return executeAreaModify(
                                    context, 's',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new CuboidArea(
                                        EXEC_POS.getX(), EXEC_POS.getY(),
                                        EXEC_POS.getZ(), getDimensionId(
                                            DimensionArgumentType
                                            .getDimensionArgument(
                                                context, "dimension"
                                            )
                                        )
                                    ), false
                                );
                            })
                        )
                    )
                    .then(
                        CommandManager
                        .argument("pos1", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("pos2", Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d POS1 =
                                Vec3ArgumentType.getVec3(context, "pos1");
                                final Vec3d POS2 =
                                Vec3ArgumentType.getVec3(context, "pos2");
                                return executeAreaModify(
                                    context, 's',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new CuboidArea(
                                        POS1.x, POS1.y, POS1.z, POS2.x, POS2.y,
                                        POS2.z,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("dimension",
                                          DimensionArgumentType.dimension())
                                .executes(context -> {
                                    final Vec3d POS1 =
                                    Vec3ArgumentType.getVec3(context, "pos1");
                                    final Vec3d POS2 =
                                    Vec3ArgumentType.getVec3(context, "pos2");
                                    return executeAreaModify(
                                        context, 's',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new CuboidArea(
                                            POS1.x, POS1.y, POS1.z, POS2.x,
                                            POS2.y, POS2.z, getDimensionId(
                                                DimensionArgumentType
                                                .getDimensionArgument(
                                                    context, "dimension"
                                                )
                                            )
                                        ), false
                                    );
                                })
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("prism")
                    .then(
                        CommandManager
                        .argument("pos1", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("pos2", Vec3ArgumentType.vec3(false))
                            .then(
                                CommandManager
                                .argument("pos3", Vec2ArgumentType.vec2(false))
                                .executes(context -> {
                                    final Vec3d POS1 =
                                    Vec3ArgumentType.getVec3(context, "pos1");
                                    final Vec3d POS2 =
                                    Vec3ArgumentType.getVec3(context, "pos2");
                                    final Vec2f POS3 =
                                    Vec2ArgumentType.getVec2(context, "pos3");
                                    return executeAreaModify(
                                        context, 's',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new TriangularPrismArea(
                                            POS1.x, POS1.y, POS1.z,
                                            POS2.x, POS2.y, POS2.z,
                                            POS3.x, POS3.y, getDimensionId(
                                                context.getSource().getWorld()
                                            )
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",
                                              DimensionArgumentType
                                              .dimension())
                                    .executes(context -> {
                                        final Vec3d POS1 =
                                        Vec3ArgumentType
                                        .getVec3(context, "pos1");
                                        final Vec3d POS2 =
                                        Vec3ArgumentType
                                        .getVec3(context, "pos2");
                                        final Vec2f POS3 =
                                        Vec2ArgumentType
                                        .getVec2(context, "pos3");
                                        return executeAreaModify(
                                            context, 's',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new TriangularPrismArea(
                                                POS1.x, POS1.y, POS1.z,
                                                POS2.x, POS2.y, POS2.z,
                                                POS3.x, POS3.y, getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("ellipsoid")
                    .then(
                        CommandManager
                        .argument("center_pos", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("axes_lengths",
                                      Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d CENTER_POS =
                                Vec3ArgumentType.getVec3(context,"center_pos");
                                final Vec3d AXES_LENGTHS =
                                Vec3ArgumentType
                                .getVec3(context, "axes_lengths");
                                return executeAreaModify(
                                    context, 's',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new EllipsoidArea(
                                        CENTER_POS.x,CENTER_POS.y,CENTER_POS.z,
                                        AXES_LENGTHS.x, AXES_LENGTHS.y,
                                        AXES_LENGTHS.z, true,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("include_face",
                                          BoolArgumentType.bool())
                                .executes(context -> {
                                    final ServerCommandSource SOURCE =
                                    context.getSource();
                                    final Vec3d CENTER_POS =
                                    Vec3ArgumentType
                                    .getVec3(context, "center_pos");
                                    final Vec3d AXES_LENGTHS =
                                    Vec3ArgumentType
                                    .getVec3(context, "axes_lengths");
                                    return executeAreaModify(
                                        context, 's',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new EllipsoidArea(
                                            CENTER_POS.x, CENTER_POS.y,
                                            CENTER_POS.z, AXES_LENGTHS.x,
                                            AXES_LENGTHS.y, AXES_LENGTHS.z,
                                            BoolArgumentType
                                            .getBool(context, "include_face"),
                                            getDimensionId(SOURCE.getWorld())
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",DimensionArgumentType
                                                          .dimension())
                                    .executes(context -> {
                                        final Vec3d CENTER_POS =
                                        Vec3ArgumentType
                                        .getVec3(context, "center_pos");
                                        final Vec3d AXES_LENGTHS =
                                        Vec3ArgumentType
                                        .getVec3(context, "axes_lengths");
                                        return executeAreaModify(
                                            context, 's',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new EllipsoidArea(
                                                CENTER_POS.x, CENTER_POS.y,
                                                CENTER_POS.z, AXES_LENGTHS.x,
                                                AXES_LENGTHS.y, AXES_LENGTHS.z,
                                                BoolArgumentType
                                                .getBool(context,
                                                         "include_face"),
                                                getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
                .then(
                    CommandManager.literal("cylinder")
                    .then(
                        CommandManager
                        .argument("bottom_pos", Vec3ArgumentType.vec3(false))
                        .then(
                            CommandManager
                            .argument("axes_lengths",
                                      Vec3ArgumentType.vec3(false))
                            .executes(context -> {
                                final ServerCommandSource SOURCE =
                                context.getSource();
                                final Vec3d BOTTOM_POS =
                                Vec3ArgumentType.getVec3(context,"bottom_pos");
                                final Vec3d AXES_LENGTHS =
                                Vec3ArgumentType
                                .getVec3(context, "axes_lengths");
                                return executeAreaModify(
                                    context, 's',
                                    StringArgumentType
                                    .getString(context, "area_name"),
                                    new EllipticCylinderArea(
                                        BOTTOM_POS.x,BOTTOM_POS.y,BOTTOM_POS.z,
                                        AXES_LENGTHS.x, AXES_LENGTHS.y,
                                        AXES_LENGTHS.z, true,
                                        getDimensionId(SOURCE.getWorld())
                                    ), false
                                );
                            })
                            .then(
                                CommandManager
                                .argument("include_side_face",
                                          BoolArgumentType.bool())
                                .executes(context -> {
                                    final ServerCommandSource SOURCE =
                                    context.getSource();
                                    final Vec3d BOTTOM_POS =
                                    Vec3ArgumentType
                                    .getVec3(context, "bottom_pos");
                                    final Vec3d AXES_LENGTHS =
                                    Vec3ArgumentType
                                    .getVec3(context, "axes_lengths");
                                    return executeAreaModify(
                                        context, 's',
                                        StringArgumentType
                                        .getString(context, "area_name"),
                                        new EllipticCylinderArea(
                                            BOTTOM_POS.x, BOTTOM_POS.y,
                                            BOTTOM_POS.z, AXES_LENGTHS.x,
                                            AXES_LENGTHS.y, AXES_LENGTHS.z,
                                            BoolArgumentType
                                            .getBool(context,
                                                     "include_side_face"),
                                            getDimensionId(SOURCE.getWorld())
                                        ), false
                                    );
                                })
                                .then(
                                    CommandManager
                                    .argument("dimension",DimensionArgumentType
                                                          .dimension())
                                    .executes(context -> {
                                        final Vec3d BOTTOM_POS =
                                        Vec3ArgumentType
                                        .getVec3(context, "bottom_pos");
                                        final Vec3d AXES_LENGTHS =
                                        Vec3ArgumentType
                                        .getVec3(context, "axes_lengths");
                                        return executeAreaModify(
                                            context, 's',
                                            StringArgumentType
                                            .getString(context, "area_name"),
                                            new EllipticCylinderArea(
                                                BOTTOM_POS.x, BOTTOM_POS.y,
                                                BOTTOM_POS.z, AXES_LENGTHS.x,
                                                AXES_LENGTHS.y, AXES_LENGTHS.z,
                                                BoolArgumentType
                                                .getBool(context,
                                                         "include_side_face"),
                                                getDimensionId(
                                                    DimensionArgumentType
                                                    .getDimensionArgument(
                                                        context, "dimension"
                                                    )
                                                )
                                            ), false
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
            )
        )
        .then(
            CommandManager.literal("notify_entering")
            .then(
                CommandManager.argument("area_name",StringArgumentType.string())
                .executes(context -> {
                    final ServerCommandSource SOURCE = context.getSource();
                    final MinecraftServer SERVER = SOURCE.getServer();
                    final Areas AREAS = Areas.getAreasRecord(SERVER);
                    final String AREA_NAME =
                    StringArgumentType.getString(context, "area_name");
                    if (AREAS == null) {
                        SOURCE.sendError(Text.literal(
                            "mod internal error: cannot get areas record"
                        ));
                        return 0;
                    }
                    final Areas.NotifyMode MODE =
                    AREAS.getNotifyEnteringMode(AREA_NAME);
                    SOURCE.sendFeedback(() -> Text.translatableWithFallback(
                        "commands.areas.success.notify_entering",
                        "Notify entering of the area is \"%s\"",
                        (MODE == null) ? "off" : MODE.name().toLowerCase()
                    ), true);
                    return (MODE == null) ? 0 : 1;
                })
                .then(
                    CommandManager.literal("all")
                    .executes(context -> executeAreaNotifyEnteringSetting(
                        context, Areas.NotifyMode.ALL
                    ))
                )
                .then(
                    CommandManager.literal("console")
                    .executes(context -> executeAreaNotifyEnteringSetting(
                        context, Areas.NotifyMode.CONSOLE
                    ))
                )
                .then(
                    CommandManager.literal("trigger")
                    .executes(context -> executeAreaNotifyEnteringSetting(
                        context, Areas.NotifyMode.TRIGGER
                    ))
                )
                .then(
                    CommandManager.literal("off")
                    .executes(context -> executeAreaNotifyEnteringSetting(
                        context, null
                    ))
                )
            )
        )
        .then(
            CommandManager.literal("notify_leaving")
            .then(
                CommandManager.argument("area_name",StringArgumentType.string())
                .executes(context -> {
                    final ServerCommandSource SOURCE = context.getSource();
                    final MinecraftServer SERVER = SOURCE.getServer();
                    final Areas AREAS = Areas.getAreasRecord(SERVER);
                    final String AREA_NAME =
                    StringArgumentType.getString(context, "area_name");
                    if (AREAS == null) {
                        SOURCE.sendError(Text.literal(
                            "mod internal error: cannot get areas record"
                        ));
                        return 0;
                    }
                    final Areas.NotifyMode MODE =
                    AREAS.getNotifyLeavingMode(AREA_NAME);
                    SOURCE.sendFeedback(() -> Text.translatableWithFallback(
                        "commands.areas.success.notify_leaving",
                        "Notify leaving of the area is \"%s\"",
                        (MODE == null) ? "off" : MODE.name().toLowerCase()
                    ), true);
                    return (MODE == null) ? 0 : 1;
                })
                .then(
                    CommandManager.literal("all")
                    .executes(context -> executeAreaNotifyLeavingSetting(
                        context, Areas.NotifyMode.ALL
                    ))
                )
                .then(
                    CommandManager.literal("console")
                    .executes(context -> executeAreaNotifyLeavingSetting(
                        context, Areas.NotifyMode.CONSOLE
                    ))
                )
                .then(
                    CommandManager.literal("trigger")
                    .executes(context -> executeAreaNotifyLeavingSetting(
                        context, Areas.NotifyMode.TRIGGER
                    ))
                )
                .then(
                    CommandManager.literal("off")
                    .executes(context -> executeAreaNotifyLeavingSetting(
                        context, null
                    ))
                )
            )
        )
        .then(
            CommandManager.literal("test")
            .then(
                CommandManager
                .argument("entities", EntityArgumentType.entities())
                .then(
                    CommandManager.literal("in")
                    .then(
                        CommandManager
                        .argument("area_name", StringArgumentType.string())
                        .executes(context -> {
                            final ServerCommandSource SOURCE =
                            context.getSource();
                            final MinecraftServer SERVER = SOURCE.getServer();
                            final Areas AREAS = Areas.getAreasRecord(SERVER);
                            final String AREA_NAME =
                            StringArgumentType.getString(context, "area_name");
                            if (AREAS == null) {
                                SOURCE.sendError(Text.literal(
                                    "mod internal error: cannot get areas record"
                                ));
                                return 0;
                            }
                            if (!AREAS.has(AREA_NAME)) {
                                throw TEST_AREA_NOT_FOUND.create(AREA_NAME);
                            }
                            final Area AREA = AREAS.getArea(AREA_NAME);
                            final Collection<? extends Entity> ETTS =
                            EntityArgumentType
                            .getOptionalEntities(context,"entities");
                            int count = 0;
                            for (Entity i : ETTS) {
                                final Vec3d POS = i.getPos();
                                final String DIM =getDimensionId(i.getWorld());
                                if (AREA.inArea(POS.x, POS.y, POS.z, DIM)) {
                                    count++;
                                }
                            }
                            final int COUNT = count;
                            if (COUNT != 0) {
                                SOURCE
                                .sendFeedback(() -> Text
                                                    .translatableWithFallback(
                                    "commands.areas.success.test.entities.in",
                                    "There are %s entity/entities in area %s",
                                    COUNT,
                                    AREAS.getColoredAreaNameText(AREA_NAME)
                                ), false);
                            } else {
                                SOURCE.sendError(Text.translatableWithFallback(
                                    "commands.areas.failed.test.entities.in",
                                    "There are 0 entities in area %s",
                                    AREA_NAME
                                ));
                            }
                            return COUNT;
                        })
                    )
                )
                .then(
                    CommandManager.literal("out_of")
                    .then(
                        CommandManager
                        .argument("area_name", StringArgumentType.string())
                        .executes(context -> {
                            final ServerCommandSource SOURCE =
                            context.getSource();
                            final MinecraftServer SERVER = SOURCE.getServer();
                            final Areas AREAS = Areas.getAreasRecord(SERVER);
                            final String AREA_NAME =
                            StringArgumentType.getString(context, "area_name");
                            if (AREAS == null) {
                                SOURCE.sendError(Text.literal(
                                    "mod internal error: cannot get areas record"
                                ));
                                return 0;
                            }
                            if (!AREAS.has(AREA_NAME)) {
                                throw TEST_AREA_NOT_FOUND.create(AREA_NAME);
                            }
                            final Area AREA = AREAS.getArea(AREA_NAME);
                            final Collection<? extends Entity> ETTS =
                            EntityArgumentType
                            .getOptionalEntities(context,"entities");
                            int count = 0;
                            for (Entity i : ETTS) {
                                final Vec3d POS = i.getPos();
                                final String DIM =getDimensionId(i.getWorld());
                                if (!AREA.inArea(POS.x, POS.y, POS.z, DIM)) {
                                    count++;
                                }
                            }
                            final int COUNT = count;
                            if (COUNT != 0) {
                                SOURCE
                                .sendFeedback(() -> Text
                                                    .translatableWithFallback(
                                    "commands.areas.success.test.entities.out_of",
                                    "There are %s entity/entities out of area %s",
                                    COUNT,
                                    AREAS.getColoredAreaNameText(AREA_NAME)
                                ), false);
                            } else {
                                SOURCE.sendError(Text.translatableWithFallback(
                                    "commands.areas.failed.test.entities.out_of",
                                    "There are 0 entities out of area %s",
                                    AREA_NAME
                                ));
                            }
                            return COUNT;
                        })
                    )
                )
            )
            .then(
                CommandManager.argument("pos", Vec3ArgumentType.vec3(false))
                .then(
                    CommandManager.literal("belongs_to")
                    .then(
                        CommandManager
                        .argument("area_name", StringArgumentType.string())
                        .executes(context -> {
                            final ServerCommandSource SOURCE =
                            context.getSource();
                            final MinecraftServer SERVER = SOURCE.getServer();
                            final Areas AREAS = Areas.getAreasRecord(SERVER);
                            final String AREA_NAME =
                            StringArgumentType.getString(context, "area_name");
                            if (AREAS == null) {
                                SOURCE.sendError(Text.literal(
                                    "mod internal error: cannot get areas record"
                                ));
                                return 0;
                            }
                            if (!AREAS.has(AREA_NAME)) {
                                throw TEST_AREA_NOT_FOUND.create(AREA_NAME);
                            }
                            final Vec3d POS =
                            Vec3ArgumentType.getVec3(context, "pos");
                            if (AREAS.getArea(AREA_NAME).inArea(
                                POS.x, POS.y, POS.z,
                                getDimensionId(SOURCE.getWorld())
                            )) {
                                SOURCE
                                .sendFeedback(() -> Text
                                                    .translatableWithFallback(
                                    "commands.areas.success.test.pos.belongs_to",
                                    "The point belongs to area %s",
                                    AREAS.getColoredAreaNameText(AREA_NAME)
                                ), false);
                                return 1;
                            }
                            SOURCE.sendError(Text.translatableWithFallback(
                                "commands.areas.failed.test.pos.belongs_to",
                                "The point does not belong to area %s",
                                AREA_NAME
                            ));
                            return 0;
                        })
                    )
                )
                .then(
                    CommandManager.literal("not_belong_to")
                    .then(
                        CommandManager
                        .argument("area_name", StringArgumentType.string())
                        .executes(context -> {
                            final ServerCommandSource SOURCE =
                            context.getSource();
                            final MinecraftServer SERVER = SOURCE.getServer();
                            final Areas AREAS = Areas.getAreasRecord(SERVER);
                            final String AREA_NAME =
                            StringArgumentType.getString(context, "area_name");
                            if (AREAS == null) {
                                SOURCE.sendError(Text.literal(
                                    "mod internal error: cannot get areas record"
                                ));
                                return 0;
                            }
                            if (!AREAS.has(AREA_NAME)) {
                                throw TEST_AREA_NOT_FOUND.create(AREA_NAME);
                            }
                            final Vec3d POS =
                            Vec3ArgumentType.getVec3(context, "pos");
                            if (AREAS.getArea(AREA_NAME).inArea(
                                POS.x, POS.y, POS.z,
                                getDimensionId(SOURCE.getWorld())
                            )) {
                                SOURCE.sendError(Text.translatableWithFallback(
                                    "commands.areas.failed.test.pos.not_belong_to",
                                    "The point belongs to area %s",
                                    AREA_NAME
                                ));
                                return 0;
                            }
                            SOURCE
                            .sendFeedback(() -> Text
                                                .translatableWithFallback(
                                "commands.areas.success.test.pos.not_belong_to",
                                "The point does not belong to area %s",
                                AREAS.getColoredAreaNameText(AREA_NAME)
                            ), false);
                            return 1;
                        })
                    )
                )
            )
        )
        .then(
            CommandManager.literal("color")
            .then(
                CommandManager
                .argument("area_name", StringArgumentType.string())
                .executes(context -> {
                    final ServerCommandSource SOURCE = context.getSource();
                    final MinecraftServer SERVER = SOURCE.getServer();
                    final Areas AREAS = Areas.getAreasRecord(SERVER);
                    if (AREAS == null) {
                        SOURCE.sendError(Text.literal(
                            "mod internal error: cannot get areas record"
                        ));
                        return 0;
                    }
                    final String AREA_NAME =
                    StringArgumentType.getString(context, "area_name");
                    final Integer COLOR = AREAS.getColor(AREA_NAME);
                    if (COLOR == null) {
                        throw COLOR_IS_UNSET.create();
                    }
                    SOURCE.sendFeedback(() -> Text.translatableWithFallback(
                        "commands.areas.success.color",
                        "Color of the area is #%s",
                        Text.literal(String.format("%06x", COLOR))
                        .setStyle(Style.EMPTY.withColor(COLOR.intValue()))
                    ), false);
                    return COLOR.intValue();
                })
                .then(
                    CommandManager.literal("unset")
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        final String AREA_NAME =
                        StringArgumentType.getString(context, "area_name");
                        final Integer COLOR = AREAS.getColor(AREA_NAME);
                        if (COLOR == null) {
                            throw COLOR_IS_ALREADY_UNSET.create();
                        }
                        AREAS.setColor(AREA_NAME, null);
                        SOURCE.sendFeedback(() -> Text.translatableWithFallback(
                            "commands.areas.success.color.unset",
                            "Color of the area is now unset"
                        ), false);
                        return COLOR.intValue();
                    })
                )
                .then(
                    CommandManager.argument("intrgb", IntegerArgumentType
                                                      .integer(0, 16777215))
                    .executes(context -> {
                        final ServerCommandSource SOURCE = context.getSource();
                        final MinecraftServer SERVER = SOURCE.getServer();
                        final Areas AREAS = Areas.getAreasRecord(SERVER);
                        if (AREAS == null) {
                            SOURCE.sendError(Text.literal(
                                "mod internal error: cannot get areas record"
                            ));
                            return 0;
                        }
                        final String AREA_NAME =
                        StringArgumentType.getString(context, "area_name");
                        final Integer COLOR = AREAS.getColor(AREA_NAME);
                        final int NEW_COLOR =
                        IntegerArgumentType.getInteger(context, "intrgb");
                        if (COLOR != null && COLOR.intValue() == NEW_COLOR) {
                            throw COLOR_UNCHANGED
                                  .create(String.format("%06x", COLOR));
                        }
                        AREAS.setColor(AREA_NAME, Integer.valueOf(NEW_COLOR));
                        SOURCE.sendFeedback(() -> Text.translatableWithFallback(
                            "commands.areas.success.color.set",
                            "Color of the area is set to #%s",
                            Text.literal(String.format("%06x", NEW_COLOR))
                            .setStyle(Style.EMPTY.withColor(NEW_COLOR))
                        ), false);
                        return 1;
                    })
                )
            )
        ));
    }

    // Authors: REGE
    // Since: 0.0.1-a6
    private static int executeAreaList(
        ServerCommandSource source, Areas areas, Collection<String> areaNames
    ) {
        final int COUNT = areaNames.size();
        final MutableText FEEDBACK = Text.empty();
        boolean bf = false;
        for (String i : areaNames) {
            if (bf) {
                FEEDBACK.append(Text.literal(", "));
            }
            FEEDBACK.append(areas.getColoredAreaNameText(i));
            bf = true;
        }
        source.sendFeedback(() -> Text.translatableWithFallback(
            "commands.areas.success.list", "There are %s area(s): %s",
            COUNT, FEEDBACK
        ), false);
        return COUNT;
    }

    // Authors: REGE
    // Since: 0.0.1-a1
    private static int executeAreaModify(
        CommandContext<ServerCommandSource> context, char opcode,
        String areaName, Area opArea, boolean alt
    ) throws CommandSyntaxException {
        final ServerCommandSource SOURCE = context.getSource();
        final MinecraftServer SERVER = SOURCE.getServer();
        final Areas AREAS = Areas.getAreasRecord(SERVER);
        final String AREA_NAME =
        StringArgumentType.getString(context, "area_name");
        if (AREAS == null) {
            SOURCE.sendError(Text.literal(
                "mod internal error: cannot get areas record"
            ));
            return 0;
        } else if (!AREAS.has(AREA_NAME)) {
            throw MODIFY_AREA_NOT_FOUND.create(AREA_NAME);
        } else {
            AREAS.setArea(areaName, new TwoAreasCombination(
                alt ? opArea : AREAS.getArea(areaName), opcode,
                alt ? AREAS.getArea(areaName) : opArea
            ));
            SOURCE.sendFeedback(() -> Text.translatableWithFallback(
                "commands.areas.success.intersect",
                "Successfully modified the area"
            ), true);
            return 1;
        }
    }

    // Authors: REGE
    // Since: 0.0.1-a3
    private static int executeAreaNotifyEnteringSetting(
        CommandContext<ServerCommandSource> context,
        @NoMc @Nullable Areas.NotifyMode mode
    ) throws CommandSyntaxException {
        final ServerCommandSource SOURCE = context.getSource();
        final MinecraftServer SERVER = SOURCE.getServer();
        final Areas AREAS = Areas.getAreasRecord(SERVER);
        final String AREA_NAME =
        StringArgumentType.getString(context, "area_name");
        if (AREAS == null) {
            SOURCE.sendError(Text.literal(
                "mod internal error: cannot get areas record"
            ));
            return 0;
        }
        final Areas.NotifyMode PREV = AREAS.setNotifyEntering(AREA_NAME, mode);
        if (PREV == mode) {
            throw NOTIFY_ENTERING_MODE_UNCHANGED
                  .create(AREA_NAME,
                          (mode == null) ? "off" : mode.name().toLowerCase());
        }
        SOURCE.sendFeedback(() -> Text.translatableWithFallback(
            "commands.areas.success.notify_entering.set",
            "Successfully set the notify entering mode of the area"
        ), true);
        return 1;
    }

    // Authors: REGE
    // Since: 0.0.1-a3
    private static int executeAreaNotifyLeavingSetting(
        CommandContext<ServerCommandSource> context,
        @NoMc @Nullable Areas.NotifyMode mode
    ) throws CommandSyntaxException {
        final ServerCommandSource SOURCE = context.getSource();
        final MinecraftServer SERVER = SOURCE.getServer();
        final Areas AREAS = Areas.getAreasRecord(SERVER);
        final String AREA_NAME =
        StringArgumentType.getString(context, "area_name");
        if (AREAS == null) {
            SOURCE.sendError(Text.literal(
                "mod internal error: cannot get areas record"
            ));
            return 0;
        }
        final Areas.NotifyMode PREV = AREAS.setNotifyLeaving(AREA_NAME, mode);
        if (PREV == mode) {
            throw NOTIFY_LEAVING_MODE_UNCHANGED
                  .create(AREA_NAME,
                          (mode == null) ? "off" : mode.name().toLowerCase());
        }
        SOURCE.sendFeedback(() -> Text.translatableWithFallback(
            "commands.areas.success.notify_leaving.set",
            "Successfully set the notify leaving mode of the area"
        ), true);
        return 1;
    }

    // Authors: REGE
    // Since: 0.0.1-a4
    public static boolean
    posInArea(CommandContext<ServerCommandSource> context, String areaNameArg,
              String posArg, @Nullable String dimensionArg)
    throws CommandSyntaxException {
        final ServerCommandSource SOURCE = context.getSource();
        final MinecraftServer SERVER = SOURCE.getServer();
        final Areas AREAS = Areas.getAreasRecord(SERVER);
        if (AREAS == null) {
            SOURCE.sendError(Text.literal(
                "mod internal error: cannot get areas record"
            ));
            return false;
        }
        final String AREA_NAME =
        StringArgumentType.getString(context, areaNameArg);
        if (!AREAS.has(AREA_NAME)) {
            return false;
        }
        final Vec3d POS = Vec3ArgumentType.getVec3(context, posArg);
        final String DIM = getDimensionId(
            (dimensionArg == null) ? SOURCE.getWorld() :
            DimensionArgumentType.getDimensionArgument(context, dimensionArg)
        );
        return AREAS.getArea(AREA_NAME).inArea(POS.x, POS.y, POS.z, DIM);
    }

    // Authors: REGE
    // Since: 0.0.1-a4
    public static boolean
    entitiesInArea(CommandContext<ServerCommandSource> context,
                   String areaNameArg, String entitiesArg, boolean requiresAll)
    throws CommandSyntaxException {
        final ServerCommandSource SOURCE = context.getSource();
        final MinecraftServer SERVER = SOURCE.getServer();
        final Areas AREAS = Areas.getAreasRecord(SERVER);
        if (AREAS == null) {
            SOURCE.sendError(Text.literal(
                "mod internal error: cannot get areas record"
            ));
            return false;
        }
        final String AREA_NAME =
        StringArgumentType.getString(context, areaNameArg);
        if (!AREAS.has(AREA_NAME)) {
            return false;
        }
        Area AREA = AREAS.getArea(AREA_NAME);
        for (Entity i : EntityArgumentType
                        .getOptionalEntities(context, entitiesArg)) {
            final Vec3d POS = i.getPos();
            final String DIM = getDimensionId(i.getWorld());
            final boolean IN_AREA = AREA.inArea(POS.x, POS.y, POS.z, DIM);
            if (requiresAll) {
                if (!IN_AREA) {
                    return false;
                }
            } else if (IN_AREA) {
                return true;
            }
        }
        return requiresAll;
    }

    // Author: REGE
    // Since: 0.0.1-a7
    @Nullable
    public static String getDimensionId(World world) {
        return world.getDimensionEntry().getKey().map(
            key -> key.getValue().toString()
        ).orElse(null);
    }
}
