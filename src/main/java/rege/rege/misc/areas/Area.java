package rege.rege.misc.areas;

/**
 * @author REGE
 * @since 0.0.1-a1
 */
public interface Area {
    /**
     * An ampty area. There is no point belongs to this area.
     * @author REGE
     * @since 0.0.1-a1
     */
    public static final Area EMPTY_AREA = (x, y, z, dim) -> false;

    /**
     * @param x The X coordinate of the tested point.
     * @param y The Y coordinate of the tested point.
     * @param z The Z coordinate of the tested point.
     * @param dim The dimension of the tested point.
     * @return A boolean reflects whether the tested point belongs to the area.
     * @author REGE
     * @since 0.0.1-a1
     */
    public boolean inArea(double x, double y, double z, String dim);
}
