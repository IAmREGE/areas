package rege.rege.misc.areas;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Unmodifiable;

/**
 * @author REGE
 * @since 0.0.1-a1
 */
@Unmodifiable
public class CuboidArea implements Area {
    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double sx;
    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double sy;
    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double sz;
    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double ex;
    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double ey;
    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double ez;
    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    public final String dim;

    /**
     * Create a cuboid area using given start point and end point in the given dimension.
     * @param startx The X coordinate of the start point.
     * @param starty The Y coordinate of the start point.
     * @param startz The Z coordinate of the start point.
     * @param endx The X coordinate of the end point.
     * @param endy The Y coordinate of the end point.
     * @param endz The Z coordinate of the end point.
     * @param dim The dimension identifier string the cuboid in.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    public CuboidArea(double startx, double starty, double startz, double endx,
                      double endy, double endz, String dim) {
        if (startx > endx) {
            this.sx = endx;
            this.ex = startx;
        } else {
            this.sx = startx;
            this.ex = endx;
        }
        if (starty > endy) {
            this.sy = endy;
            this.ey = starty;
        } else {
            this.sy = starty;
            this.ey = endy;
        }
        if (startz > endz) {
            this.sz = endz;
            this.ez = startz;
        } else {
            this.sz = startz;
            this.ez = endz;
        }
        this.dim = dim;
    }

    /**
     * Create a cuboid area using given integral blockpos in the given
     * dimension.
     * @param x The X position of the blockpos.
     * @param y The Y position of the blockpos.
     * @param z The Z position of the blockpos.
     * @param dim The dimension identifier string the cuboid in.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    public CuboidArea(int x, int y, int z, String dim) {
        this(x, y, z, x + 1., y + 1., z + 1., dim);
    }

    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    @Override
    public boolean inArea(double x, double y, double z, String dim) {
        return dim.equals(this.dim) && this.sx <= x && x <= this.ex &&
               this.sy <= y && y <= this.ey && this.sz <= z && z <= this.ez;
    }
}
