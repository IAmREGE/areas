package rege.rege.misc.areas;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Unmodifiable;

/**
 * @author REGE
 * @since 0.0.1-a2
 */
@Unmodifiable
public class EllipsoidArea implements Area {
    /**
     * The X coordinate of the center of the ellipsoid.
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double centerX;
    /**
     * The Y coordinate of the center of the ellipsoid.
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double centerY;
    /**
     * The Z coordinate of the center of the ellipsoid.
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double centerZ;
    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double semiXAxisLength;
    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double semiYAxisLength;
    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double semiZAxisLength;
    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    public final boolean includeFace;
    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    public final String dim;

    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    @Contract(pure = true)
    public EllipsoidArea(double centerX, double centerY, double centerZ,
                         double semiXAxisLength, double semiYAxisLength,
                         double semiZAxisLength, boolean includeFace,
                         String dim) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.centerZ = centerZ;
        this.semiXAxisLength =
        (semiXAxisLength < 0) ? -semiXAxisLength : semiXAxisLength;
        this.semiYAxisLength =
        (semiYAxisLength < 0) ? -semiYAxisLength : semiYAxisLength;
        this.semiZAxisLength =
        (semiZAxisLength < 0) ? -semiZAxisLength : semiZAxisLength;
        this.includeFace = includeFace;
        this.dim = dim;
    }

    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    @Override
    public boolean inArea(double x, double y, double z, String dim) {
        final double XA = (x - this.centerX) / this.semiXAxisLength;
        final double YA = (y - this.centerY) / this.semiYAxisLength;
        final double ZA = (z - this.centerZ) / this.semiZAxisLength;
        return dim.equals(this.dim) &&
               (this.includeFace ? (XA * XA) + (YA * YA) + (ZA * ZA) <= 1 :
                ((XA * XA) + (YA * YA) + (ZA * ZA) < 1));
    }
}
