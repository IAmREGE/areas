package rege.rege.misc.areas;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Unmodifiable;

/**
 * @author REGE
 * @since 0.0.1-a2
 */
@Unmodifiable
public class EllipticCylinderArea implements Area {
    /**
     * The X coordinate of the center of the bottom of the cylinder.
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double bottomX;
    /**
     * The Y coordinate of the center of the bottom of the cylinder.
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double bottomY;
    /**
     * The Z coordinate of the center of the bottom of the cylinder.
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double bottomZ;
    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double semiXAxisLength;
    /**
     * The height of the cylinder.
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double height;
    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    public final double semiZAxisLength;
    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    public final boolean includeSideFace;
    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    public final String dim;

    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    @Contract(pure = true)
    public EllipticCylinderArea(double bottomX, double bottomY, double bottomZ,
                                double semiXAxisLength, double height,
                                double semiZAxisLength,boolean includeSideFace,
                                String dim) {
        this.bottomX = bottomX;
        if (height < 0) {
            this.bottomY = bottomY - height;
            this.height = -height;
        } else {
            this.bottomY = bottomY;
            this.height = height;
        }
        this.bottomZ = bottomZ;
        this.semiXAxisLength =
        (semiXAxisLength < 0) ? -semiXAxisLength : semiXAxisLength;
        this.semiZAxisLength =
        (semiZAxisLength < 0) ? -semiZAxisLength : semiZAxisLength;
        this.includeSideFace = includeSideFace;
        this.dim = dim;
    }

    /**
     * @author REGE
     * @since 0.0.1-a2
     */
    @Override
    public boolean inArea(double x, double y, double z, String dim) {
        final double XA = (x - this.bottomX) / this.semiXAxisLength;
        final double ZA = (z - this.bottomZ) / this.semiZAxisLength;
        return dim.equals(this.dim) && this.bottomY <= y &&
               y <= (this.bottomY + this.height) &&
               (this.includeSideFace ? (XA * XA) + (ZA * ZA) <= 1 :
                ((XA * XA) + (ZA * ZA) < 1));
    }
}
