package rege.rege.misc.areas;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Unmodifiable;

/**
 * @author REGE
 * @since 0.0.1-a1
 */
@Unmodifiable
public class TriangularPrismArea implements Area {
    /**
     * @param x1 The X coordinate of the 1st point.
     * @param y1 The Y coordinate of the 1st point.
     * @param x2 The X coordinate of the 2nd point.
     * @param y2 The Y coordinate of the 2nd point.
     * @return The amount of cross product.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    public static double
    crossProduct(double x1, double y1, double x2, double y2) {
        return x1 * y2 - x2 * y1;
    }

    /**
     * The X coordinate of the 1st point of the triangles.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double x1;
    /**
     * The bottom of the prism.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double y1;
    /**
     * The Z coordinate of the 1st point of the triangles.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double z1;
    /**
     * The X coordinate of the 2nd point of the triangles.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double x2;
    /**
     * The top of the prism.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double y2;
    /**
     * The Z coordinate of the 2nd point of the triangles.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double z2;
    /**
     * The X coordinate of the 3rd point of the triangles.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double x3;
    /**
     * The Z coordinate of the 3rd point of the triangles.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final double z3;
    /**
     * The dimension identifier string the prism in.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final String dim;

    /**
     * @param x1 The X coordinate of the 1st point of the triangles.
     * @param y1 The bottom of the prism.
     * @param z1 The Z coordinate of the 1st point of the triangles.
     * @param x2 The X coordinate of the 2nd point of the triangles.
     * @param y2 The top of the prism.
     * @param z2 The Z coordinate of the 2nd point of the triangles.
     * @param x3 The X coordinate of the 3rd point of the triangles.
     * @param z3 The Z coordinate of the 3rd point of the triangles.
     * @param dim The dimension identifier string the prism in.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    public TriangularPrismArea(double x1, double y1, double z1, double x2,
                               double y2, double z2, double x3, double z3,
                               String dim) {
        if (crossProduct(x3 - x1, z3 - z1, x2 - x1, z2 - z1) >= 0)
        {
            final double TMPX = x2;
            final double TMPZ = z2;
            x2 = x3;
            z2 = z3;
            x3 = TMPX;
            z3 = TMPZ;
        }
        this.x1 = x1;
        this.z1 = z1;
        this.x2 = x2;
        this.z2 = z2;
        this.x3 = x3;
        this.z3 = z3;
        if (y1 > y2) {
            this.y1 = y2;
            this.y2 = y1;
        } else {
            this.y1 = y1;
            this.y2 = y2;
        }
        this.dim = dim;
    }

    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    @Override
    public boolean inArea(double x, double y, double z, String dim)
    {
        return this.dim.equals(dim) && this.y1 <= y && y <= this.y2 &&
               crossProduct(this.x2 - this.x1, this.z2 - this.z1, x - this.x1, z - this.z1) >= 0 &&
               crossProduct(this.x3 - this.x2, this.z3 - this.z2, x - this.x2, z - this.z2) >= 0 &&
               crossProduct(this.x1 - this.x3, this.z1 - this.z3, x - this.x3, z - this.z3) >= 0;
    }
}
