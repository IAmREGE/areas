package rege.rege.misc.areas;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * @author REGE
 * @since 0.0.1-a1
 */
public class TwoAreasCombination implements Area {
    /**
     * Combination opcode for intersect.
     * @author REGE
     * @since 0.0.1-a1
     */
    public static final char INTERSECT = 'i';
    /**
     * Combination opcode for union.
     * @author REGE
     * @since 0.0.1-a1
     */
    public static final char UNION = 'u';
    /**
     * Combination opcode for difference.
     * @author REGE
     * @since 0.0.1-a1
     */
    public static final char DIFFERENCE = 'd';
    /**
     * Combination opcode for symmetric difference.
     * @author REGE
     * @since 0.0.1-a1
     */
    public static final char SYMMETRIC_DIFFERENCE = 's';

    /**
     * The left area to be operated.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final @NotNull Area area1;
    /**
     * The opcode.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final char combination_opcode;
    /**
     * The right area to be operated.
     * @author REGE
     * @since 0.0.1-a1
     */
    public final Area area2;

    /**
     * @param area1 The left area to be operated.
     * @param combinationOpcode The opcode.
     * @param area2 The right area to be operated.
     * @author REGE
     * @since 0.0.1-a1
     */
    @Contract(pure = true)
    public TwoAreasCombination(@NotNull Area area1, char combinationOpcode,
                               Area area2) {
        this.area1 = area1;
        this.combination_opcode = combinationOpcode;
        this.area2 = area2;
    }

    /**
     * @author REGE
     * @since 0.0.1-a1
     */
    @Override
    public boolean inArea(double x, double y, double z, String dim) {
        switch (this.combination_opcode) {
            case 'i': {
                return this.area1.inArea(x, y, z, dim) &&
                       this.area2.inArea(x, y, z, dim);
            }
            case 'u': {
                return this.area1.inArea(x, y, z, dim) ||
                       this.area2.inArea(x, y, z, dim);
            }
            case 'd': {
                return this.area1.inArea(x, y, z, dim) &&
                       !this.area2.inArea(x, y, z, dim);
            }
            case 's': {
                return this.area1.inArea(x, y, z, dim) !=
                       this.area2.inArea(x, y, z, dim);
            }
            default: return this.area1.inArea(x, y, z, dim);
        }
    }
}
