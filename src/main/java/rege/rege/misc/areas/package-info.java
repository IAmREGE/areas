/**
 * This package temporarily provides some area types for this mod.
 * Finally it will be replaced with a standalone library.
 * @author REGE
 * @since 0.0.1-a1
 */
package rege.rege.misc.areas;
